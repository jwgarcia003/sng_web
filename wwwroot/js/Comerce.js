﻿class Comerce {
    constructor(ComerceId, Name, Description, Address, Ruc, Rating, UrlLogo, CreatedAt, UpdatedAt, Longitude, Latitude,
        IsAproved, IsActived, CategoryId, OwnerId) {
        this.ComerceId = ComerceId;
        this.Name;
        this.Description;
        this.Address;
        this.Ruc;
        this.Rating;
        this.UrlLogo;
        this.CreatedAt;
        this.UpdatedAt;
        this.Longitude;
        this.Latitude;
        this.IsAproved;
        this.IsActived;
        this.CategoryId;
        this.OwnerId; 
    }

    addComerce(url) {
        var self = this;

        var comerceName = $("#ComerceName").val();
        var comerceDescription = $("#ComerceDescription").val();
        var comerceAddress = $("#ComerceAddress").val();
        var comerceRuc = $("#ComerceRuc").val();
        var comerceUrlLogo = $("#ComerceUrlLogo").val();
        var comerceCategoryid = $("#ComerceCategoryid").val();
        var comerceOwnerId = $("#ComerceOwnerId").val();

        var code = "";
        var description = "";

        $.ajax({
            type: "POST",
            url: url,
            data: {
                comerceName,
                comerceDescription,
                comerceAddress,
                comerceRuc, 
                comerceUrlLogo,
                comerceCategoryid,
                comerceOwnerId
            },
            success: function (response) {
                //console.log(response);
                $.each(response, (index, value) => {
                    code = value.code;
                    description = value.description;
                });
                if (code == "Saved") {
                    self.loadTable();
                }
            },
            error: function (err) {
                console.log("Error: ");
                console.log(err);
            }
        });
    }

    getComerce() {
        var self = this;
        $.ajax({
            type: "POST",
            url: url,
            data: { id },
            success: function (response) {
                self.setData(response);
            },
            error: function (err) {
                console.log("Error: " + err);
            }
        });
    }

    getOnlyComerce() {
        var result = "";
        $.ajax({
            type: "GET",
            url: "/Comerces/GetOnlyComerces",
            data: {},
            success: function (response) {
                result = (response);
            },
            error: function (err) {
                console.log("Error: " + err);
            }
        });
        return result;
    }

    

    readURLComerce(input, ComerceBanner) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                ComerceBanner.attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    loadComercesTable(option) {
        var comerces_data = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = $.Deferred(),
                    args = {};

                if (loadOptions.sort) {
                    args.orderby = loadOptions.sort[0].selector;
                    if (loadOptions.sort[0].desc)
                        args.orderby += option;
                }

                args.skip = loadOptions.skip || 0;
                args.take = loadOptions.take || 10;
                args.orderby = loadOptions.orderby || option;

                $.ajax({
                    url: "/Comerces/GetComerces",
                    dataType: "json",
                    data: args,
                    success: function (result) {
                        console.log("Get Comerces...");
                        console.log(result)
                        deferred.resolve(result, { totalCount: result.length });
                    },
                    error: function (err) {
                        deferred.reject("Problemas al cargar los datos de comercios");
                    },
                    timeout: 15000
                });

                return deferred.promise();
            },

            insert: function (values) {
                console.log(values);
                var data = {
                    "Name": values.name,
                    "Description": values.description,
                    "Address": values.address,
                    "Ruc": values.ruc,
                    "UrlLogo": values.ulrImage || "/images/comerce.png",
                    "Categoryid": values.categoryId || 1,
                    "OwnerId": values.ownerId || 1
                };
                console.log("data to save ")
                console.log(data);

                return $.ajax({
                    url: "Comerces/AddComerce",
                    method: "POST",
                    data: data,
                    success: function (res) {
                        $.each(res, (index, value) => {
                            DevExpress.ui.notify(value.description, "success", 3000);
                        });
                    },
                    error: function (err) {
                        console.log("error...  " + err)
                        $.each(err, (index, value) => {
                            DevExpress.ui.notify("Error al ingresar nuevo producto", "error", 3000);
                        });
                    }
                })
            },

            update: function (key, values) {
                var comerce = JSON.parse(JSON.stringify(key));
                console.log("current comerce...")
                console.log(comerce)
                var data = {
                    "idComerce": comerce.comerceId,
                    "Name": (values.name != null && values.name != "") ? values.name : comerce.name,
                    "Description": (values.description != null && values.description != "") ? values.description : comerce.description,
                    "Address": (values.address != null && values.address != "") ? values.address : comerce.address,
                    "Ruc": (values.ruc != null && values.ruc != "") ? values.name : comerce.ruc,
                    "UrlLogo": (values.urlLogo != null && values.urlLogo != "") ? values.urlLogo : "/images/comerce.png", //comerce.urlLogo,
                    "CategoryId": (values.categoryId != null && values.Categoryid > 0) ? values.Categoryid : comerce.categoryId,
                    "OwnerId": (values.ownerId != null && values.ownerId > 0) ? values.ownerId : comerce.ownerId,
                    "CreatedAt": (values.createdAt != null && values.createdAt != "") ? values.createdAt : comerce.createdAt,
                    "IsActived": (values.isActived != null) ? values.isActived : comerce.createdAt,
                    "IsAproved": (values.isAproved != null) ? values.isAproved : comerce.createdAt,
                };
                console.log("Data to edit from comerce...")
                console.log(data);

                return $.ajax({
                    url: "Comerces/UpdateComerce/" + comerce.comerceId,
                    method: "PUT",
                    data: data,
                    success: function (res) {
                        $.each(res, (index, value) => {
                            DevExpress.ui.notify(value.description, "info", 3000);
                        });
                    },
                    error: function (err) {
                        $.each(err, (index, value) => {
                            DevExpress.ui.notify("Error al actualizar datos del producto", "error", 3000);
                        });
                    }
                })
            },

            remove: function (key) {
                var product = JSON.parse(JSON.stringify(key));
                return $.ajax({
                    url: "/Comerces/DeleteProduct/" + product.productId,
                    method: "DELETE",
                    data: {
                        "id": product.productId,
                        "name": product.name
                    },
                    success: function (res) {
                        $.each(res, (index, value) => {
                            console.log(value.description)
                            DevExpress.ui.notify(value.description, "warning", 3000);
                        });
                    },
                    error: function (err) {
                        $.each(err, (index, value) => {
                            DevExpress.ui.notify("Error al eliminar producto", "error", 3000);
                        });
                    }
                })
            },
        });

        $("#comerce_container_data").dxDataGrid({
            dataSource: comerces_data,
            accessKey: "description",
            allowColumnResizing: true,
            columnResizingMode: "nextColumn",
            columnMinWidth: 50,
            wordWrapEnabled: true,
            showBorders: false,
            groupPanel: {
                visible: true,
                emptyPanelText: "Arrastrar y soltar una columna...",
            },
            grouping: {
                texts: {
                    groupContinuesMessage: "Continua en la siguiente página >",
                    groupContinuedMessage: "< Continua en la pagina anterios",
                }
            },
            loadPanel: {
                height: 100,
                width: 300,
                text: "Cargando...",
            },
            searchPanel: {
                visible: true,
                resizeEnabled: true,
                width: 400,
                placeholder: "Buscar comercio..."
            },
            selection: {
                mode: "single"
            },
            onSelectionChanged: function (selectedItems) {
                console.log(comerce);
                var comerce = selectedItems.selectedRowsData[0];
                window.location = '/Comerces/Details/' + comerce.comerceId;
            },
            remoteOperations: false,
            paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 30]
            },
            columns: [
                {
                    dateFiel: "urlLogo",
                    caption: "Logo",
                    width: 100,
                    allowFiltering: false,
                    allowSorting: false,
                    dataType: "file",
                    alignment: "center",
                    cellTemplate: function (container, options) {
                        var logo = options.data.urlLogo != null ? (options.data.urlLogo) : "/images/comerce.png";
                        $("<div>")
                            .append($("<img>", {
                                "src": logo,
                                "width": "50",
                                "alt": "Logo",
                            }))
                            .appendTo(container);
                    }
                },
                {
                    dataField: "category",
                    caption: "Categoria",
                    width: 200,
                    calculateCellValue: function (rowData) {
                        return ((rowData.category != null) ? rowData.category.name : "categoria no disponible");
                    }
                },
                


                {
                    dataField: "name",
                    caption: "Comercio",
                    width: 200
                },
                {
                    dataField: "rating",
                    caption: "Puntuación",
                    width: 100,
                    alignment: "center",
                    validationRules: [
                        {
                            type: "range",
                            message: "La puntuación debe estar entre 1 y 5",
                            min: 1,
                            max: 5
                        }],
                    label: { align: "center" }
                },
                {
                    dataField: "createdAt",
                    caption: "Afiliado desde",
                    minWidth: 150,
                    calculateCellValue: function (rowData) {
                        return dateFormat(rowData.createdAt);
                    },
                },
                {
                    dataField: "address",
                    caption: "Visto",
                    width: 80,
                    alignment: "center",
                    calculateCellValue: function (rowData) {
                        return "5 veces";
                    },
                },
                {
                    dataField: "isAproved",
                    caption: "Publicado",
                    width: 100,
                    calculateCellValue: function (rowData) {
                        return rowData.isActived;
                    },
                },
                {
                    dataField: "isActived",
                    caption: "Activo",
                    width: 100,
                    calculateCellValue: function (rowData) {
                        return rowData.isActived;
                    },
                }
            ],
            //....................................................................................................................................
            //...................Product container................................................................................................
            //....................................................................................................................................
            masterDetail: {
                enabled: true,
                template: function (container, response) {
                    //console.log("masterDetail...")
                    //console.log(response)
                    var comerce = response.data;
                    var category = comerce.category;
                    var owner = comerce.owner;
                    var products = comerce.products;

                    $("<div>"
                        + "<div id='left-" + comerce.comerceId + "' class='col-sm-3'></div>"
                        + "<div id='right-" + comerce.comerceId + "' class='col-sm-9'></div>"
                        + "</div>").appendTo(container);

                    var lefth = $("<div>"
                        + "<h4>Propietario:<br/><small>" + owner.fullName + "</small></h4>"
                        + "</div>")
                        .addClass("master-detail-caption")
                        .appendTo($("#left-" + comerce.comerceId + ""));

                    var leftb = $("<hr /><div>     "
                        +"<div>                                       "
                        +"    <div class='uk-panel'>                  "
                        +"        <dl class='uk-description-list'>    "
                        + "            <dt><h4>Detalle</h4></dt>             "
                        + "            <dd><p>" + comerce.description + "</p></dd>                      "
                        +"        </dl>                               "
                        + "    </div><br>                                  "

                        + "    <div class='uk-panel'>                  "
                        + "        <dl class='uk-description-list'>    "
                        + "            <dt><h4>Ubicación</h4></dt>         "
                        + "            <dd><p>" + comerce.address + "</p></dd>                   "
                        + "        </dl>                               "
                        + "    </div><br>                                  "

                        + "    <div class='uk-panel'>                  "
                        + "        <dl class='uk-description-list'>    "
                        + "            <dt><h4>RUC</h4></dt>                  "
                        + "            <dd>" + comerce.ruc + "</dd>                "
                        + "        </dl>                               "
                        + "    </div><br>                                  "
                        + "</div>                                      "

                        + "</div><hr />").appendTo($("#left-" + comerce.comerceId + ""));

                    $("<div id='products_data'>").dxDataGrid({
                        dataSource: products,
                        accessKey: "description",
                        allowColumnResizing: true,
                        columnResizingMode: "nextColumn",
                        columnMinWidth: 50,
                        wordWrapEnabled: true,
                        showBorders: true,
                        searchPanel: {
                            visible: true,
                            width: 300,
                            placeholder: "Buscar producto..."
                        },
                        selection: {
                            mode: "single"
                        },
                        onSelectionChanged: function (selectedItems) {
                            var data = selectedItems.selectedRowsData[0];
                            showProductPopup(data);
                            console.log(data)
                        },
                        remoteOperations: {
                            sorting: true,
                            paging: true
                        },
                        paging: {
                            pageSize: 5
                        },
                        pager: {
                            showPageSizeSelector: true,
                            allowedPageSizes: [5, 10, 20]
                        },
                        editing: {
                            mode: "popup",
                            //allowUpdating: true,
                            //allowDeleting: true,
                            //allowAdding: true,
                            popup: {
                                title: "Información del producto",
                                showTitle: true,
                                width: 700,
                                height: 500,
                                resizable: true,
                                position: {
                                    my: "top",
                                    at: "top",
                                    of: window
                                },
                            },

                        },
                        columns: [
                            {
                                dateFiel: "urlImage",
                                caption: "Imagen",
                                width: 90,
                                allowFiltering: false,
                                allowSorting: false,
                                alignment: "center",
                                cellTemplate: function (container, options) {
                                    $("<div>")
                                        .append($("<img>", {
                                            "src": options.data.urlImage,
                                            "width": "50",
                                            "alt": "Imagen"
                                        }))
                                        .appendTo(container);
                                }
                            },
                            {
                                dataField: "comerceId",
                                caption: "Código comercio",
                                width: 70,
                                alignment: "center",
                                visible: false, // this field is hidden
                            },
                            {
                                dataField: "productId",
                                caption: "Código",
                                width: 90,
                                alignment: "center",
                                sortOrder: "asc",
                            },
                            {
                                dataField: "name",
                                caption: "Nombre",
                                width: 150,
                                sortOrder: "asc",

                            },
                            {
                                dataField: "description",
                                caption: "Descripción",
                                sortOrder: "desc",
                                minWidth: 200,
                            },
                            {
                                dataField: "price",
                                caption: "Precio",
                                format: "currency",
                                alignment: "right",
                                calculateCellValue: function (rowData) {
                                    return "C$ " + rowData.price;
                                },
                                precision: 2,
                                width: 80,
                            },
                            {
                                dataField: "isActived",
                                caption: "Habilitado",
                                dataType: "boolean",
                                calculateCellValue: function (rowData) {
                                    return rowData.isActived;
                                },
                                width: 90,
                            },
                        ],

                    })
                    .appendTo($("#right-" + comerce.comerceId + ""));
                }
            }
        });

    }

    setTabPanel() {
        DevExpress.ui.setTemplateEngine("underscore");
        var tabPanelItems = [{
            "ID": 1,
            "CompanyName": "SuprMart",
            "Address": "702 SW 8th Street",
            "City": "Bentonville",
            "State": "Arkansas",
            "Zipcode": 72716,
            "Phone": "(800) 555-2797",
            "Fax": "(800) 555-2171",
            "Website": "http://www.nowebsitesupermart.com"
        }, {
            "ID": 2,
            "CompanyName": "El'Depot",
            "Address": "2455 Paces Ferry Road NW",
            "City": "Atlanta",
            "State": "Georgia",
            "Zipcode": 30339,
            "Phone": "(800) 595-3232",
            "Fax": "(800) 595-3231",
            "Website": "http://www.nowebsitedepot.com"
        }, {
            "ID": 3,
            "CompanyName": "K&S Music",
            "Address": "1000 Nicllet Mall",
            "City": "Minneapolis",
            "State": "Minnesota",
            "Zipcode": 55403,
            "Phone": "(612) 304-6073",
            "Fax": "(612) 304-6074",
            "Website": "http://www.nowebsitemusic.com"
        }, {
            "ID": 4,
            "CompanyName": "Tom Club",
            "Address": "999 Lake Drive",
            "City": "Issaquah",
            "State": "Washington",
            "Zipcode": 98027,
            "Phone": "(800) 955-2292",
            "Fax": "(800) 955-2293",
            "Website": "http://www.nowebsitetomsclub.com"
        }];
        var tabPanel = $("#tabpanel-container").dxTabPanel({
            height: 260,
            dataSource: tabPanelItems,
            selectedIndex: 0,
            loop: false,
            animationEnabled: true,
            swipeEnabled: true,
            itemTitleTemplate: $("#title"),
            itemTemplate: $("#customer"),
            onSelectionChanged: function (e) {
                $(".selected-index")
                    .text(e.component.option("selectedIndex") + 1);
            }
        }).dxTabPanel("instance");


        $("#loop-enabled").dxCheckBox({
            value: false,
            text: "Loop enabled",
            onValueChanged: function (e) {
                tabPanel.option("loop", e.value);
            }
        });

        $("#animation-enabled").dxCheckBox({
            value: true,
            text: "Animation enabled",
            onValueChanged: function (e) {
                tabPanel.option("animationEnabled", e.value);
            }
        });

        $("#swipe-enabled").dxCheckBox({
            value: true,
            text: "Swipe enabled",
            onValueChanged: function (e) {
                tabPanel.option("swipeEnabled", e.value);
            }
        });

        $(".item-count").text(tabPanelItems.length);

    }



    //........................
    setData(response) {
        $.each(response, (index, value) => {
            //setter data modal
            $("#Name").val(value.name);
           


            //set data object
            this.Name = value.name;


            console.log(response)
        });
    }

    loadTable() {
        var self = this;
        $.ajax({
            type: "GET",
            url: "/Comerces/GetComerces",
            data: {},
            success: function (response) {
                var table = $("#table_comerce");
                var row = "";
                var modal = $("#modal_newcomerce");
                table.html("");
                console.log(response)
                $.each(response, (index, item) => {
                    console.log(item)
                    row = "<tr style='vertical-align: middle'>"
                        + "    <td>"
                        + "    <img alt = 'image' width = '50' class='uk-border-squart' src='http://lorempixel.com/400/200'>"
                        + "    </td>"
                        + "    <td class='uk-table-expand'>"
                        + item.name
                        + "    </td>"
                        + "    <td class='uk-table-expand'>"
                        + item.description
                        + "    </td>"
                        + "    <td class='uk-table-expand'>"
                        + item.address
                        + "    </td>"
                        + "    <td>"
                        + item.ruc
                        + "    </td>"
                        + "    <td class='center'>"
                        + item.rating
                        + "    </td>"
                        + "    <td class='uk-text-truncate' uk-tooltip='title: " + item.owner.name + " " + item.owner.lastName + "; pos: top-right'>"
                        + item.owner.name + " " + item.owner.lastName
                        + "    </td>"
                        + "    <td class='center'>"
                        + "        <div class='checkbox checbox-switch switch-primary'>"
                        + "            <label>"
                        + "                <input type = 'checkbox' name='' checked='" + item.isAproved + "' value='" + item.isAproved + "' disabled />"
                        + "                <span></span>"
                        + "            </label>"
                        + "        </div>"
                        + "    </td>"
                        + "    <td class='center'>"
                        + "        <div class='checkbox checbox-switch switch-primary'>"
                        + "            <label>"
                        + "                <input type = 'checkbox' name='' checked='" + item.isActived + "' value='" + item.isActived + "' disabled />"
                        + "                <span></span>"
                        + "            </label>"
                        + "        </div>"
                        + "    </td>"
                        + "    <td class='td-options center'>"
                        + "        <a class='btn btn-xs btn-success' asp-action='Edit' asp-route-id='" + item.comerceId + "' uk-tooltip='title: Editar datos del comercio'><span class=' glyphicon glyphicon-edit'></span></a>"
                        + "        <a class='btn btn-xs btn-info' asp-action='Details' asp-route-id='" + item.comerceId + "' uk-tooltip='title: Detalle del comercio'><span class='glyphicon glyphicon-list'></span></a>"
                        + "        <a class='btn btn-xs  btn-danger' asp-action='Delete' asp-route-id='" + item.comerceId + "' uk-tooltip='title: Eliminar registro'><span class='glyphicon glyphicon-trash'></span></a>"
                        + "    </td>"
                        + "</tr>";

                    table.append(row);
                    self.clear();
                });
                modal.hide();
            },
            error: function (err) {
                console.log("Error: " + err);
            }
        });
    }

    getComerceFilter(pageNumber, value) {
        var action = "Comerces/GetComerceFilter";
        if (value == "")
            value = "null";
        console.log("value comerce to find: " + value)

        $.ajax({
            type: "POST",
            url: action,
            data: { pageNumber, value },
            success: function (response) {
                $("#table_comerce").html("");

                $.each(response, (index, val) => {
                    console.log(response);
                    $("#table_comerce").html(val[0]);
                });
            },
            error: function (err, options, throws) {
                console.log(err);
            }
        });
    }

    clear() {
        $("#ComerceName").html("");
        $("#ComerceDescription").html("");
        $("#ComerceAddress").html("");
        $("#ComerceRuc").html("");
        $("#ComerceUrlLogo").html("");
        $("#ComerceCategoryid").html("");
        $("#ComerceOwnerId").html("");
    }

}