﻿class Product {
    constructor(ProductId, Name, Description, Price, UrlImage, CreatedAt, UpdatedAt, IsActived, ComerceId) {
        this.ProductId = ProductId;
        this.Name = Name;
        this.Description = Description;
        this.Price = Price;
        this.UrlImage = UrlImage;
        this.CreatedAt = CreatedAt;
        this.UpdatedAt = UpdatedAt;
        this.IsActived = IsActived;
        this.ComerceId = ComerceId;
    }

    addProduct(url, form) {
        var self = this;
        var modal = UIkit.modal($("#new_product"));
        var code = "";
        var description = "";

        var p = $("#ProductPrice").val();
        var precio = ((p==null || p=="") ? 0 : p );

        var form_data = new FormData();
        form_data.append('Name', $("#ProductName").val());
        form_data.append('Description', $("#ProductDescription").val());
        form_data.append('Price', precio);
        form_data.append('Currency', $("#currency").val());
        form_data.append('ComerceId', $("#ComerceId").val());

        jQuery.each($("#ProductUrlImage")[0].files, function(i, file) {
            console.log('UrlImage',file);
            form_data.append(i, file);
        }); 

        $.ajax({
            type: "POST",
            url: url,
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function(response) {
                //console.log(response);
                $.each(response,
                    (index, value) => {
                        code = value.code;
                        description = value.description;
                    });
                if (code == "Saved") {
                    modal.hide();
                    DevExpress.ui.notify({
                        message: description || "¡Registro guardado con exito!",
                        type: "info",
                        displayTime: 5000,
                        closeOnClick: true
                    });
                    $(':input',form)
                        .removeAttr('checked')
                        .removeAttr('selected')
                        .not(':button, :submit, :reset, :hidden, :radio, :checkbox')
                        .val('');
                    $('#ProductBannerNew').attr('src','/images/no-image.svg');
                    self.loadProductsTable();
                }
            },
            error: function(err) {
                console.log("Error: ");
                console.log(err);
                modal.hide();
                DevExpress.ui.notify({
                    message: "No fue posible agregar el producto :(",
                    type: "error",
                    displayTime: 5000,
                    closeOnClick: true
                });
            }
        });
    }
    
    //Metodo de prueba para subir fotos
    UploadFilesAjax(url) {
        var self = this;
        var modal_product = $("#new_product");

        var Name = $("#ProductName").val();
        var Description = $("#ProductDescription").val();
        var Price = $("#ProductPrice").val();
        var UrlImage = $('#ProductUrlImage').val();
        var ComerceId = $("#ComerceId").val();

        var fileupload = $("#ProductUrlImage").get(0);
        var files = fileupload.files;

        var data = new FormData($("ProductForm").serialize());
        for (var i = 0; i < files.length; i++) {
            data.append(files[i].name, files[i]);
        }

        var product = {
            Name: Name,
            Description: Description,
            Price: Price,
            ComerceId: ComerceId
        }

        data.append("Product", product);

        console.log("data.......................");
        console.log(data);

        $.ajax({
            method: 'POST',
            type: 'POST', 
            url: "/Products/uploadfilesajax",
            contenttype: false,
            processdata: false,
            cache: false,
            data: data,
            success: function (message) {
                console.log(message);
                modal_product.hide();
                self.loadproductstable();
            },
            error: function (err) {
                console.log(err);
                alert("ocurrió un error al ingresar datos del producto");
            }
        });
    }
    //....................................................

    getProducts() {
        var self = this;
        $.ajax({
            type: "POST",
            url: url,
            data: { id },
            success: function(response) {
                self.setData(response);
            },
            error: function(err) {
                console.log("Error: " + err);
            }
        });
    }

    readURLProduct(input, ProductBanner) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                ProductBanner.attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    //Data Grid Products ..................................................................................................................................
    loadProductsTable(req) {
        var products_data = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = $.Deferred(),
                    args = {};
                var OrderBy, Skip, Take, Url, data;
                
                if (loadOptions.sort) {
                    args.orderby = loadOptions.sort[0].selector;
                    OrderBy = loadOptions.sort[0].selector;
                    if (loadOptions.sort[0].desc)
                        args.orderby += " desc";
                }

                if (req === "getAll") {
                    Url = "/Products/GetProducts";
                    //console.log("getAllProducts...", Url)
                } else {
                    Url = "/Products/GetProductsByComerce";
                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 5;
                    args.orderby = loadOptions.orderby || "desc";
                    args.comerceId = ComerceId;
                    //console.log("getProductsByComerce...", Url)
                }
                //console.log("args: " + JSON.stringify(args));

                $.ajax({
                    url: Url,
                    dataType: "json",
                    data: args,
                    success: function (result) {
                        if (req === "getByComerce")
                            json_product = result;
                        deferred.resolve(result, { totalCount: result.length });
                        console.log("All products available");
                        console.log(result);
                    },
                    error: function (err) {
                        //console.log(err)
                        deferred.reject("Problemas al cargar lista de productos");
                    },
                    timeout: 10000
                });

                return deferred.promise();
            },

        });

        $("#table_products").dxDataGrid({
            dataSource: products_data,
            accessKey: "description",
            allowColumnResizing: true,
            columnResizingMode: "nextColumn",
            columnMinWidth: 50,
            wordWrapEnabled: true,
            showBorders: false,
            groupPanel: {
                visible: true,
                emptyPanelText: "Arrastrar y soltar una columna...",
            },
            grouping: {
                texts: {
                    groupContinuesMessage: "Continua en la siguiente página >",
                    groupContinuedMessage: "< Continua en la pagina anterios",
                }
            },
            searchPanel: {
                visible: true,
                width: 300,
                placeholder: "Buscar producto..."
            },
            loadPanel: {
                height: 100,
                width: 300,
                text: "Cargando...",
            },
            selection: {
                mode: "single"
            },
            onSelectionChanged: function (selectedItems) {
                //var data = selectedItems.selectedRowsData[0];
                //showProductPopup(data);
                //console.log(data)
            },
            onRowClick: function (component) {
                var data = component.data
                showProductPopup(data);
                //console.log(component.data)
            },
            remoteOperations: false,
            //remoteOperations: {
            //    sorting: true,
            //    paging: true
            //},
            paging: {
                enabled: true,
                pageSize: 5
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20]
            },
            columns: [
                {
                    dataFiel: "urlImage",
                    caption: "Imagen",
                    width: 80,
                    allowFiltering: false,
                    allowSorting: false,
                    alignment: "center",
                    cellTemplate: function (container, options) {
                        if (options.data.urlImage) {
                            $("<div>")
                                .append($("<img>", {
                                    "src": options.data.urlImage,
                                    "width": "50",
                                    "alt": "Imagen"
                                }))
                                .appendTo(container);
                        } else {
                            $("<div>")
                                .append($("<img>", {
                                    "src": "/images/product.jpg",
                                    "width": "50",
                                    "alt": "Imagen"
                                }))
                                .appendTo(container);
                        }
                    }
                }, {
                    dataField: "comerce",
                    caption: "Comercio",
                    minWidth: 150,
                    visible: (req === "getAll"),
                    //calculateCellValue: function (rowData) {
                    //    var comerceName = (rowData.comerce != null) ? rowData.comerce.name : "Comercio no disponible";
                    //    return comerceName;
                    //},
                    minWidth: 150,
                    visible: (req === "getAll"),
                    //calculateCellValue: function (rowData) {
                    //    var comerceName = (rowData.comerce != null) ? rowData.comerce.name : "Comercio no disponible";
                    //    return comerceName;
                    //},
                    cellTemplate: function (container, options) {
                        var data = {};
                        if (options.data.comerce != null) {
                            data = {
                                name: options.data.comerce.name,
                                urlImage: options.data.comerce.UrlLogo,
                            }
                        } else {
                            data = {
                                name: "Comercio no disponible",
                                urlImage: "/images/comerce.png",
                            }
                        }
                        $("<div>")
                            .append($("<img>", {
                                "src": data.urlImage,
                                "width": "50",
                                "alt": ""
                            }).css({ "float": "left" }))
                            .append($("<p>" + data.name + "</p>")
                                .css({ "padding-top": "15px" }))
                            .appendTo(container);
                    }
                },
                {
                    dataField: "name",
                    caption: "Nombre",
                    width: 150,
                    sortOrder: "asc",
                    allowSorting: true,
                },
                {
                    dataField: "description",
                    caption: "Descripción",
                    alignment: "justify",
                    sortOrder: "desc",
                    minWidth: 200,
                },
                {
                    dataField: "price",
                    caption: "Precio",
                    precision: 2,
                    calculateCellValue: function (rowData) {
                        var c = rowData.currency != null ? rowData.currency : "C$";
                        return c + " " + rowData.price;
                    },
                    width: 100,
                    alignment: "right",
                },
                {
                    dataField: "isActived",
                    caption: "Habilitado",
                    dataType: "boolean",
                    calculateCellValue: function (rowData) {
                        return rowData.isActived;
                    },
                    width: 100,
                    alignment: "center",
                },
            ],
            });
    }
    
    setData(response) {
        $.each(response,
            (index, value) => {
                //setter data modal
                $("#Name").val(value.name);


                //set data object
                this.Name = value.name;


                console.log(response)
            });
    }

    
    //......................................................................................................................................................
    getProductFilter(pageNumber, value) {
        var action = "/Products/GetProductFilter"
        if (value == "") 
            value = "null";
        console.log("value product to find: " + value);

        $.ajax({
            type: "POST",
            url: action,
            data: { pageNumber, value },
            success: function(response) {
                $("#table_product").html("");

                $.each(response,
                    (index, val) => {
                        console.log(response);
                        $("#table_product").html(val[0]);
                    });
            },
            error: function(err, options, throws) {
                console.log(err);
            }
        });

    }


    clear() {
        $("#ProductName").html("");
        $("#ProductDescription").html("");
        $("#ProductPrice").html("");
        $("#ProductComerceid").html("");
    }

    loadTable() {
        var self = this;
        $.ajax({
            type: "GET",
            url: "/Products/GetProducts",
            data: {},
            success: function(response) {
                var table = $("#table_product");
                var content = "";
                var modal = $("#modal_newproduct");
                table.html("");
                console.log(response);
                $.each(response,
                    (index, value) => {
                        content = "<tr>" +
                            "    <td>" +
                            "     <img alt='image' width='50px' class='img-thumbnail img-responsive ' src='http://lorempixel.com/400/200'>" +
                            "    </td>" +
                            "    <td>" +
                            value.Name +
                            "    </td>" +
                            "    <td>" +
                            value.Description +
                            "    </td>" +
                            "    <td>" +
                            value.Price +
                            "    </td>" +
                            "    <td>" +
                            value.IsActived +
                            "    </td>" +
                            "    <td>" +
                            value.Comerce.Name +
                            "    </td>" +
                            "    <td>" +
                            "        <a asp-action='Edit' asp-route-id='" +
                            value.ProductId +
                            "'>Edit</a> |" +
                            "        <a asp-action='Details' asp-route-id='" +
                            value.ProductId +
                            "'>Details</a> |" +
                            "        <a asp-action='Delete' asp-route-id='" +
                            value.ProductId +
                            "'>Delete</a>" +
                            "    </td>" +
                            "</tr>";
                        table.append(content);
                        modal.hice();
                        self.clear();
                    });
            },
            error: function(err) {
                console.log("Error: " + err);
            }
        });
    }

}