﻿class Cupon {
    constructor() {
        this.datareport = [];
    }

    addCupon() {
        var self = this;
        var Descount = $("#CuponDescount").val();
        var Validity = $("#CuponValidity").val();
        var MoneyValue = $("#CuponMoneyValue").val();
        var ComerceId = $("#ComerceId").val();

        var code = "";
        var description = "";
        var modal = UIkit.modal($("#new_cupon"));

        $.ajax({
            type: "POST",
            url: "/Cupons/AddCupon",
            data: {
                Descount,
                Validity,
                MoneyValue,
                ComerceId
            },
            success: function (response) {
                //console.log(response);
                $.each(response,
                    (index, value) => {
                        code = value.code;
                        description = value.description;
                    });
                if (code == "Saved") {
                    modal.hide();
                    DevExpress.ui.notify({
                        message: description || "¡Registro guardado con exito!",
                        type: "info",
                        displayTime: 5000,
                        closeOnClick: true
                    });
                    self.loadCuponsTable();
                }
            },
            error: function (err) {
                console.log("Error: ");
                console.log(err);
            }
        });
    }

    getData(req, isReport) {
        var self = this;
        var cupons_data = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = $.Deferred();

                var OrderBy, Skip, Take, Url, data;
                var groupOptions = loadOptions.group ? JSON.stringify(loadOptions.group) : ""; // Getting group options
                if (loadOptions.sort) {
                    OrderBy = loadOptions.sort[0].selector;
                    if (loadOptions.sort[0].desc)
                        OrderBy += " desc";
                }

                Skip = loadOptions.skip || 0;
                Take = loadOptions.take || 5;

                if (req === "getAll") {
                    //console.log("getAll...")
                    Url = "/Cupons/GetCupons";
                    data = {}
                } else {
                    //console.log("getByComerce...")
                    Url = "/Cupons/GetCuponsByComerce";
                    data = {
                        "ComerceId": ComerceId,
                        "skip": Skip,
                        "take": Take,
                        "orderBy": OrderBy
                    }
                }

                $.ajax({
                    url: Url,
                    dataType: "json",
                    data: data,
                    success: function (result) {
                        //self.datareport = JSON.stringify(result);
                        if (isReport==true) {
                            var cupons = [];
                            $.each(result, (index, p) => {
                                if (p.isChanged == true) {
                                    cupons.push(p);
                                }
                            });
                            //self.datareport = (promotions);
                            deferred.resolve(cupons, { totalCount: cupons.length });
                        } else {
                            //self.datareport = (result);
                            deferred.resolve(result, { totalCount: result.length });
                        }
                        //console.log("Get cupons from server...");
                        //console.log(result)
                    },
                    error: function () {
                        deferred.reject("Problemas al cargar lista de cupones");
                    },
                    timeout: 5000
                });

                return deferred.promise();
            },

        });
        return cupons_data;
    }

    getCuponsChart(req, isReport) {
        var params = {}, json = [], url;
        if (req === "getAll") {
            url = "/Cupons/GetCupons"
        } else {
            url = "/Cupons/GetCuponsByComerce";
            params = {
                "ComerceId": ComerceId,
                "skip": 5,
                "take": 5,
                "orderBy": ""
            }
        }

        $.ajax({
            url: url,
            dataType: "json",
            data: params,
            success: function (result) {
                //console.log(result)
                if (isReport == true) {
                    var res = [];
                    $.each(result, (index, p) => {
                        if (p.isChanged == true) {
                            var cupon = {
                                date: dateFormatShort(p.updatedAt),
                                moneyValue: p.moneyValue,
                                count: p.cuponId,
                            }
                            res.push(cupon);
                        }
                    });
                    json = res;
                } else {
                    json = result;
                }
            },
            error: function () {
            },
            async: false
        });
        return json;
    }

    loadCuponsTable(req) {
        var self = this;
        var data = self.getData(req, false);
        

        $("#table_cupons").dxDataGrid({
            dataSource: data,
            accessKey: "name",
            allowColumnResizing: true,
            wordWrapEnabled: true,
            columnResizingMode: "nextColumn",
            columnMinWidth: 50,
            showBorders: false,
            groupPanel: {
                visible: true,
                emptyPanelText: "Arrastrar y soltar una columna...",
            },
            grouping: {
                texts: {
                    groupContinuesMessage: "Continua en la siguiente página >",
                    groupContinuedMessage: "< Continua en la pagina anterios",
                }
            },
            searchPanel: {
                visible: true,
                width: 300,
                placeholder: "Buscar cupón..."
            },
            loadPanel: {
                height: 100,
                width: 300,
                text: "Cargando...",
            },
            selection: {
                mode: "single"
            },
            onSelectionChanged: function (selectedItems) {
                //var cupon = selectedItems.selectedRowsData[0];
                //showCuponPopup(cupon);
                //console.log(cupon)
            },
            onRowClick: function (component) {
                var data = component.data
                showCuponPopup(data);
                //console.log(component.data)
            },
            remoteOperations: false,
            paging: {
                pageSize: 5
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20]
            },
            columns: [
                {
                    dataField: "comerceId",
                    caption: "Comercio",
                    minWidth: 200,
                    visible: (req === "getAll"),
                    //calculateCellValue: function (rowData) {
                    //    var comerceName = (rowData.comerce != null) ? rowData.comerce.name : "Comercio no disponible";
                    //    return comerceName;
                    //},
                    cellTemplate: function (container, options) {
                        var data = {};
                        if (options.data.comerce != null) {
                            data = {
                                name: options.data.comerce.name,
                                urlImage: options.data.comerce.UrlLogo,
                            }
                        } else {
                            data = {
                                name: "Comercio no disponible",
                                urlImage: "/images/comerce.png",
                            }
                        }
                        $("<div>")
                            .append($("<img>", {
                                "src": data.urlImage,
                                "width": "50",
                                "alt": ""
                            }).css({ "float": "left" }))
                            .append($("<p>" + data.name + "</p>")
                                .css({ "padding-top": "15px" }))
                            .appendTo(container);
                    }
                },
                {
                    dataField: "cuponId",
                    caption: "Código",
                    alignment: "center",
                    width: 100,
                    sortOrder: "asc",
                },
                {
                    dataField: "descount",
                    caption: "Descuento",
                    sortOrder: "desc",
                    minWidth: 100,
                    customizeText: function (cellInfo) {
                        return (cellInfo.value * 100) + " %";
                    }
                },
                {
                    dataField: "moneyValue",
                    caption: "Valor Monetario",
                    format: "currency",
                    minWidth: 100,
                    sortOrder: "asc",
                    customizeText: function (cellInfo) {
                        return " C$ "+(cellInfo.value)  ;
                    }
                },
                {
                    dataField: "validity",
                    caption: "Periodo",
                    minWidth: 80,
                    customizeText: function (cellInfo) {
                        return cellInfo.value + " días";
                    }
                },
                {
                    dataField: "isChanged",
                    caption: "Canjeado",
                    dataType: "boolean",
                    width: 80,
                    calculateCellValue: function (rowData) {
                        return rowData.isChanged;
                    },
                },
                {
                    dataField: "isActive",
                    caption: "Habilitado",
                    dataType: "boolean",
                    width: 80,
                    calculateCellValue: function (rowData) {
                        return rowData.isActive;
                    },
                },
            ],
        });
    }

    loadCuponsReport(req) {
        var self = this;
        var data = self.getData(req, true);

        var response = [];
        response = self.getCuponsChart(req, true);
        var groupBy = function (data, prop) {
            return data.reduce(function (groups, item) {
                var val = item[prop];
                groups[val] = groups[val] || { date: item.date, moneyValue: 0, count: 0 };
                groups[val].date = item.date;
                groups[val].moneyValue += item.moneyValue;
                groups[val].count += 1;
                return groups;
            }, []);
        }
        var dataSource = Object.values(groupBy(response, 'date'));

        //console.log(dataSource)

        $("#table_reportCupons").dxDataGrid({
            dataSource: data,
            accessKey: "name",
            allowColumnResizing: true,
            wordWrapEnabled: true,
            columnResizingMode: "nextColumn",
            columnMinWidth: 50,
            showBorders: false,
            searchPanel: {
                visible: false,
                width: 300,
                placeholder: "Buscar cupón..."
            },
            selection: {
                mode: "multiple"
            },
            "export": {
                enabled: true,
                fileName: "Cupones canjeados",
                allowExportSelectedData: true
            },
            groupPanel: {
                visible: true,
                emptyPanelText: "Arrastrar y soltar una columna para realizar un filtro de cupones..."
            },
            remoteOperations: false,
            paging: {
                pageSize: 5
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20]
            },
            columns: [
                {
                    dataField: "comerceId",
                    caption: "Comercio",
                    minWidth: 100,
                    visible: (req === "getAll"),
                    calculateCellValue: function (rowData) {
                        var comerceName = (rowData.comerce != null) ? rowData.comerce.name : "Comercio no disponible";
                        return comerceName;
                    },
                },
                {
                    dataField: "cuponId",
                    caption: "Código",
                    alignment: "center",
                    width: 100,
                    sortOrder: "asc",
                    visible: false
                },
                {
                    dataField: "isChanged",
                    caption: "Canjeado",
                    dataType: "boolean",
                    width: 75,
                    visible: false,
                    calculateCellValue: function (rowData) {
                        return rowData.isChanged;
                    },
                },
                {
                    dataField: "descount",
                    caption: "Descuento",
                    sortOrder: "desc",
                    //alignment: "center",
                    width: 90,
                    customizeText: function (cellInfo) {
                        return (cellInfo.value * 100) + " %";
                    }
                },
                {
                    dataField: "moneyValue",
                    caption: "Valor Monetario",
                    format: "currency",
                    minWidth: 100,
                    sortOrder: "asc",
                    customizeText: function (cellInfo) {
                        return " C$ " + (cellInfo.value);
                    }
                },
                {
                    dataField: "updatedAt",
                    caption: "Actualizado",
                    minWidth: 80,
                    alignment: "center",
                    customizeText: function (cellInfo) {
                        return dateFormatShort(cellInfo.value);
                    }
                },
                {
                    dataField: "validity",
                    caption: "Periodo",
                    width: 70,
                    customizeText: function (cellInfo) {
                        return cellInfo.value + " días";
                    }
                },
                {
                    dataField: "isActive",
                    caption: "Habilitado",
                    dataType: "boolean",
                    visible: false,
                    width: 80,
                    calculateCellValue: function (rowData) {
                        return rowData.isActive;
                    },
                },
            ],
        });

        self.setChart($("#chart_reportCupons"), dataSource);
        //self.setChart_ResolvedAxisLabelOverlapping($("#chart_reportPromotions"), $("#overlapping-modes"), dataSource, "count", "Cupones Canjeados")
        
    }

    setChart(container, data) {
        var types = ["spline", "stackedspline", "fullstackedspline"];  
        var chart = container.dxChart({
            title: "Cupones de Descuento",
            palette: "deepskyblue",
            dataSource: data,
            commonSeriesSettings: {
                type: types[0],
                argumentField: "date"
            },
            commonAxisSettings: {
                grid: {
                    visible: true
                }
            },
            margin: {
                bottom: 20
            },
            series: [
                { valueField: "count", name: "Canjeados" },
                //{ valueField: "moneyValue", name: "Valor Monetario" },
            ],
            tooltip: {
                enabled: true
            },
            legend: {
                verticalAlignment: "top",
                horizontalAlignment: "right"
            },
            "export": {
                enabled: false
            },
            argumentAxis: {
                label: {
                    format: {
                        type: "decimal"
                    }
                },
                allowDecimals: false,
                axisDivisionFactor: 1
            },
            
        }).dxChart("instance");

        $("#types").dxSelectBox({
            dataSource: types,
            value: types[0],
            onValueChanged: function (e) {
                chart.option("commonSeriesSettings.type", e.value);
            }
        });

    }

    setChart_ResolvedAxisLabelOverlapping(container, containerMode, dataSource, argumentField, title, ) {
        var overlappingModes = ["stagger", "rotate", "hide", "none"]
        var chart = container.dxChart({
            dataSource: dataSource,
            series: [{
                argumentField: argumentField
            }],
            argumentAxis: {
                label: {
                    overlappingBehavior: overlappingModes[0]
                }
            },
            legend: {
                visible: false
            },
            title: title
        }).dxChart("instance");

        containerMode.dxSelectBox({
            dataSource: overlappingModes,
            value: overlappingModes[0],
            onValueChanged: function (e) {
                chart.option("argumentAxis.label.overlappingBehavior", e.value);
            }
        });
    }

    
}