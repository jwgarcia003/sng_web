﻿class User {
    constructor() { }

    changePassword() {
        var self = this;
        var loadPanel = $(".loadpanel").dxLoadPanel({
            //shadingColor: "rgba(0,0,0,0.4)",
            position: { of: "#panel" },
            visible: false,
            showIndicator: true,
            showPane: true,
            shading: true,
            message: "Actualizando...",
            closeOnOutsideClick: false,
            onShown: function () {
                self.updating();
                setTimeout(function () {
                    loadPanel.hide();
                }, 3000);
            },
            onHidden: function () {

            }
        }).dxLoadPanel("instance");

        loadPanel.show();
    }

    updating() {
        var self = this;
        var code = "";
        var description = "";
        var Url = "/AccountController/ChangePasswordAjax";

        var data = {
            id: $("#Id").val(),
            newpassword: $("#ChangedPassword").val()
        }
        //console.log("data: " + JSON.stringify(data));
        $.ajax({
            type: "POST",
            url: "/Account/ChangePasswordAjax",
            data: data,
            success: function (response) {
                //console.log(response);
                $.each(response,
                    (index, value) => {
                        code = value.code;
                        description = value.description;
                    });
                if (code == "Saved") {
                    console.log("¡La Contaseña ha Sido Cambiada exitosamente!")
                    DevExpress.ui.notify({
                        message: description || "¡La Contaseña ha Sido Cambiada exitosamente!",
                        type: "info",
                        displayTime: 5000,
                        closeOnClick: true,
                        position: {
                            my: 'top center',
                            at: 'top center',
                            of: window,
                            offset: '0 60'
                        },
                        width: "500px"
                    });
                }
                if (code == "Error") {
                    console.log("¡Contraseña no pudo ser actualizada!")
                    DevExpress.ui.notify({
                        message: description || "¡Contraseña no pudo ser actualizada!",
                        type: "warning",
                        displayTime: 5000,
                        closeOnClick: true,
                        position: {
                            my: 'top center',
                            at: 'top center',
                            of: window,
                            offset: '0 60'
                        },
                        width: "500px"
                    });
                }
            },
            error: function (err) {
                console.log("Error: ");
                console.log(err);
                DevExpress.ui.notify({
                    message: description || "¡Error al actualizadar contraseña!",
                    type: "error",
                    displayTime: 5000,
                    closeOnClick: true,
                    position: {
                        my: 'top center',
                        at: 'top center',
                        of: window,
                        offset: '0 60'
                    },
                    width: "500px"
                });
            }
        });
    };

    checkConfirmPassword() {
        console.log("checkConfirmPassword...")
        var self = this;
        if ($('#ChangedPassword').val() === $('#confirmChangedPassword').val()) {
            $('#message').html('Contraseña verificada').css('color', 'green');
            //console.log("actualizando")
            self.changePassword();
        } else {
            $('#message').html('Verificar las contraseñas').css('color', 'red');
            //console.log("error ,no actualizando")
        }
    }
}