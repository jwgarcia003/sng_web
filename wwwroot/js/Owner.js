﻿class Owner {
    constructor(OwnerId, Name, LastName, Address, Phone, IsActive) {
        this.OwnerId = OwnerId;
        this.Name = Name;
        this.LastName = LastName;
        this.Address = Address;
        this.Phone = Phone;
        this.IsActive = IsActive;
    }

    getOwners() {
        $.ajax({
            type: "GET",
            url: "/Owners/GetOwners",
            data: {},
            success: function (response) {
                console.log(response)
                var select = $("#ComerceOwnerId");
                select.html("");
                select.append("<option value='0' disabled>" + " -Propietario del comercio- " + "</option>");

                $.each(response, (index, value) => {
                    select.append("<option value=" + value.ownerId + ">" + value.fullName + "</option>");
                });
            },
            error: function (err) {
                console.log("Error: " + err);
            }
        });
    }

}