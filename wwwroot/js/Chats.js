class Chats {
  constructor() {}

  SetChatsVisto(destinatarioId, comerceId) {
    self = this;
    $.ajax({
      type: "GET",
      url: "/Chats/SetChatsVisto",
      contentType: "application/json",
      dataType: "json",
      data: {
        destinatarioId,
        comerceId,
      },
      success: function (response) {
        console.log("\n\nsetChatVisto response :\n", JSON.stringify(response));
      },
      error: function (err) {
        console.log(JSON.stringify(err));
        console.log("Error to set chats vistos from server:\n\n" + err);
      },
    });
  }

  getChatsCommerce(commerce_id) {
    self = this;
    $.ajax({
      type: "GET",
      url: "/Chats/GetChats/?comerceId=" + commerce_id,
      contentType: "application/json",
      dataType: "json",
      data: {},
      success: function (response) {
        response = response.filter(function (entry) {
          console.log(entry);
          if (entry) {
            return entry;
          }
        });

        response = self.order(response);
        response = self.orderByVisto(response);
        //console.log('GetChats\n', JSON.stringify(response))

        var content = $("#chats-list");
        content.html("");

        $.each(response, (index, value) => {
          if (value) {
            //console.log(index, value);
            var message =
              value.mensajes.length > 0
                ? value.mensajes[value.mensajes.length - 1].mensaje
                : undefined;
            var user_name =
              value.user.firstName ||
              "" + value.user.lastName ||
              "" ||
              "Usuario";
            var user_avatar = value.user.avatarURL || "/images/user.png";
            var last_message = message || "Mensaje";
            var route_id_chat =
              "/Chats/Details/" + value.id + "?userId=" + value.user.id;
            var hasNotSeen = self.hasNotSeen(value);

            var html =
              `<li class="item" style="padding: 10px !important">                        
                            <div class="product-img">
                                <img class='direct-chat-img' src='` +
              user_avatar +
              `' alt='image'>
                            </div>
                            <div class="product-info">
                                <a href="` +
              route_id_chat +
              `" class="product-title">
                                    ` +
              user_name +
              `
                                <span class="product-description">
                                    ` +
              last_message +
              `
                                </span>
                            </div>
                        </li>`;

            var htmlNoVisto =
              `<li class="item" style="background: #ecf0f5 !important; padding: 20px 10px !important">                        
                            <div class="product-img">
                                <img class='direct-chat-img' src='` +
              user_avatar +
              `' alt='image'>
                            </div>
                            <div class="product-info">
                                <a href="` +
              route_id_chat +
              `" class="product-title">
                                    ` +
              user_name +
              `
                                <span class="product-description">
                                    ` +
              last_message +
              `
                                </span>
                            </div>
                        </li>`;

            if (hasNotSeen) content.append(htmlNoVisto);
            else content.append(html);
          }
        });
      },
      error: function (err) {
        console.log(JSON.stringify(err));
        console.log("Error to get chats from server:\n\n" + err);
      },
    });
  }

  getChats() {
    self = this;
    $.ajax({
      type: "GET",
      url: "/Chats/GetChats",
      contentType: "application/json",
      dataType: "json",
      data: {},
      success: function (response) {
        response = response.filter(function (entry) {
          console.log(entry);

          if (entry) {
            return entry;
          }
        });

        response = self.order(response);
        response = self.orderByVisto(response);
        //console.log('\n\nGetChats response .............\n', JSON.stringify(response), '\n\n')
        var content = $("#chats-list");
        content.html("");

        $.each(response, (index, value) => {
          if (value) {
            //console.log(index, value);
            var message =
              value.mensajes.length > 0
                ? value.mensajes[value.mensajes.length - 1].mensaje
                : undefined;
            var user_name =
              value.user.firstName || "" + value.user.lastName || "";
            var user_avatar = value.user.avatarURL || "/images/user.png";
            var last_message = message || "";
            var route_id_chat = "/Chats/Details/" + value.id;
            var hasNotSeen = self.hasNotSeen(value);

            var html =
              `<li class="item" style="padding: 10px !important">                        
                            <div class="product-img">
                                <img class='direct-chat-img' src='` +
              user_avatar +
              `' alt='image'>
                            </div>
                            <div class="product-info">
                                <a href="` +
              route_id_chat +
              `" class="product-title">
                                    ` +
              user_name +
              `
                                <span class="product-description">
                                    ` +
              last_message +
              `
                                </span>
                            </div>
                        </li>`;

            var htmlNoVisto =
              `<li class="item" style="background: #ecf0f5 !important; padding: 20px 10px !important">                        
                            <div class="product-img">
                                <img class='direct-chat-img' src='` +
              user_avatar +
              `' alt='image'>
                            </div>
                            <div class="product-info">
                                <a href="` +
              route_id_chat +
              `" class="product-title">
                                    ` +
              user_name +
              `
                                <span class="product-description">
                                    ` +
              last_message +
              `
                                </span>
                            </div>
                        </li>`;

            if (hasNotSeen) content.append(htmlNoVisto);
            else content.append(html);
          }
        });
      },
      error: function (err) {
        console.log(JSON.stringify(err));
        console.log("Error to get chats from server:\n\n" + err);
      },
    });
  }

  order(data) {
    return data.sort(function (c1, c2) {
      var last_message_1 =
        c1.mensajes.length > 0
          ? c1.mensajes[c1.mensajes.length - 1]
          : undefined;
      //console.log('\last_message_1: ', c1)
      var last_message_date_1 = last_message_1.fecha;
      var date_1 = new Date(last_message_date_1);

      var last_message_2 =
        c2.mensajes.length > 0
          ? c2.mensajes[c2.mensajes.length - 1]
          : undefined;
      var last_message_date_2 = last_message_2.fecha;
      var date_2 = new Date(last_message_date_2);
      return date_1 > date_2 ? -1 : date_1 < date_2 ? 1 : 0;
      //return true;
    });
  }

  orderByVisto(data) {
    return data.sort(function (data1, data2) {
      var contain1 = data1.mensajes.map(function (message) {
        return message.visto ? true : false;
      });

      var contain2 = data2.mensajes.map(function (message) {
        return message.visto ? true : false;
      });

      return contain1 - contain2;
    });
  }

  hasNotSeen(data) {
    var hasNotSeen = data.mensajes.some(function (message) {
      if (message.client) {
        if (message.visto == undefined) return false;
        else return !message.visto;
      } else {
        return false;
      }
    });
    return hasNotSeen;
  }

  sendChats(message, user_id, comerce_id) {
    var self = this;

    var mensaje = message;
    var destinatarioId = user_id;
    var comerceId = comerce_id;

    $.ajax({
      type: "POST",
      url: "/Chats/SendChats",
      data: {
        mensaje,
        destinatarioId,
        comerceId,
      },
      success: function (response) {
        console.log(response);
      },
      error: function (err) {
        console.log(JSON.stringify(err));
        console.log("Error to get chats from server:\n\n" + err);
      },
    });
  }

  formatDate(response) {
    var date = new Date(response);
    var monthNames = [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre",
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();

    var ampm = hours >= 12 ? "pm" : "am";
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? "0" + minutes : minutes;
    var strTime = hours + ":" + minutes + " " + ampm;

    return day + " " + monthNames[monthIndex] + " " + year + ", " + strTime;
  }
}
