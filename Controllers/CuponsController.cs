﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OneSignal.CSharp.SDK;
using OneSignal.CSharp.SDK.Resources;
using OneSignal.CSharp.SDK.Resources.Notifications;
using sng_web.Common;
using sng_web.Data;
using sng_web.Models;
using sng_web.Services;

namespace sng_web.Controllers
{
    public class CuponsController : Controller
    {
        private readonly INotifications _notification;
        private readonly ApplicationDbContext _context;

        private const string AppId = "41f069e7-5b25-429f-b807-c2a92696041e";

        public CuponsController(ApplicationDbContext context, INotifications notification)
        {
            _context = context;
            _notification = notification;
        }

        // POST: add new product 
        // Ajax method
        public async Task<List<IdentityError>> AddCupon(Double Descount, int Validity, Decimal MoneyValue, int ComerceId)
        {
            List<IdentityError> errors = new List<IdentityError>();
            Cupon Cupon = new Cupon()
            {
                Descount = Descount,
                Validity = Validity,
                MoneyValue = MoneyValue,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                isActive = true,
                IsChanged = false,
                ComerceId = ComerceId
            };
            Cupon.Descount /= 100;
            _context.Add(Cupon);
            await _context.SaveChangesAsync();

            DataNotification data = new DataNotification
            {
                Model = "cupon",
                ObjectId = Cupon.CuponId.ToString(),
                ComerceId = Cupon.ComerceId.ToString()
            };
            await _notification.CreateNotification("¡Nuevo cupon de descuento disponible!", data);

            errors.Add(new IdentityError()
            {
                Code = "Saved",
                Description = "Cupón guardado exitosamente."
            });
            return errors;
        }


        public async Task<ICollection<Cupon>> GetCupons()
        {
            var cupons = await _context.Cupon.Include(c => c.Comerce)
                                            .OrderBy(p => p.ComerceId)
                                            .ToListAsync();
            List<Cupon> list = new List<Cupon>();
            foreach (var cupon in cupons)
            {
                cupon.Comerce = await _context.Comerce.FirstOrDefaultAsync(c => c.ComerceId == cupon.ComerceId);
                list.Add(cupon);
            }

            return (list);
        }

        public async Task<ICollection<Cupon>> GetCuponsByComerce(int ComerceId, int skip, int take, string orderby)
        {
            var cupons = await _context.Cupon.Where(p => p.ComerceId == ComerceId)
                                                    .Include(c => c.Comerce)
                                                    .OrderBy(p => p.CuponId)
                                                    .ToListAsync();
            List<Cupon> list = new List<Cupon>();
            foreach (var cupon in cupons)
            {
                cupon.Comerce = await _context.Comerce.FirstOrDefaultAsync(c => c.ComerceId == ComerceId);
                list.Add(cupon);
            }

            return (list);
        }

        // GET: Cupons
        [Authorize(Roles="Admin")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Cupon.ToListAsync());
        }

        // GET: Cupons/Details/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cupon = await _context.Cupon
                .SingleOrDefaultAsync(m => m.CuponId == id);
            if (cupon == null)
            {
                return NotFound();
            }

            return View(cupon);
        }

        // GET: Cupons/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            ViewData["ComerceId"] = new SelectList(_context.Comerce, "ComerceId", "Name");
            return View();
        }

        // POST: Cupons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("CuponId,Descount,Validity,MoneyValue,CreatedAt,UpdatedAt,IsChanged,isActive,ComerceId")] Cupon cupon)
        {
            if (ModelState.IsValid)
            {
                cupon.Descount /= 100;
                cupon.CreatedAt = DateTime.UtcNow;
                cupon.UpdatedAt = DateTime.UtcNow;
                _context.Add(cupon);
                await _context.SaveChangesAsync();

                if( cupon.isActive && cupon.Comerce.IsAproved){
                    var data = new Dictionary<string,string>(){
                        {"Model", "cupon"},
                        {"ObjectId", cupon.CuponId.ToString()},
                        {"ComerceId", cupon.ComerceId.ToString()}
                    };
                    await _notification.CreateNotification(cupon.Comerce.Name + "Ha publicado un nuevo cupon!", data);
                }
                

                return RedirectToAction(nameof(Index));
            }
            return View(cupon);
        }

        // GET: Cupons/Edit/5
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var cupon = await _context.Cupon.SingleOrDefaultAsync(m => m.CuponId == id);
            if (cupon == null)
                return NotFound();

            ViewBag.Refer = Request.Headers["Referer"].ToString();

            //Validate authentication from user
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (!User.IsInRole("Admin"))
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                if (comerce_claim == null || Convert.ToInt32(comerce_claim.Value) != cupon.ComerceId)
                    return NotFound();
            }
            cupon.Descount *= 100;
            return View(cupon);
        }

        // POST: Cupons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Edit(int id, [Bind("CuponId,Descount,Validity,MoneyValue,CreatedAt,UpdatedAt,IsChanged,isActive,ComerceId")] Cupon cupon)
        {
            ViewData["ReturnUrl"] = "/Comerces/Details/" + cupon.ComerceId;
            if (id != cupon.CuponId)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    cupon.Descount /= 100;
                    cupon.UpdatedAt = DateTime.Now;
                    _context.Update(cupon);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CuponExists(cupon.CuponId))
                        return NotFound();
                    else
                        throw;
                }

                if (Request.Form["Refer"].ToString() != "")
                    return Redirect(Request.Form["Refer"].ToString());
                else
                    return RedirectToAction(nameof(Index));
            }
            return View(cupon);
        }

        // GET: Cupons/Delete/5
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var cupon = await _context.Cupon
                .SingleOrDefaultAsync(m => m.CuponId == id);
            if (cupon == null)
                return NotFound();

            ViewBag.Refer = Request.Headers["Referer"].ToString();

            //Validate authentication from user
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (!User.IsInRole("Admin"))
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                if (comerce_claim == null || Convert.ToInt32(comerce_claim.Value) != cupon.ComerceId)
                    return NotFound();
            }

            return View(cupon);
        }

        // POST: Cupons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cupon = await _context.Cupon.SingleOrDefaultAsync(m => m.CuponId == id);
            ViewData["ReturnUrl"] = "/Comerces/Details/" + cupon.ComerceId;
            //_context.Cupon.Remove(cupon);
            //await _context.SaveChangesAsync();
            
            if (Request.Form["Refer"].ToString() != "")
                return Redirect(Request.Form["Refer"].ToString());
            else
                return RedirectToAction(nameof(Index));
        }

        private bool CuponExists(int id)
        {
            return _context.Cupon.Any(e => e.CuponId == id);
        }

        private IActionResult RedirectToMyAction()
        {
            if (!User.IsInRole("Admin"))
            {
                return Redirect(ViewData["ReturnUrl"].ToString());
            }
            else
            {
                return RedirectToAction(nameof(PromotionsController.Index), "Cupons");
            }
        }
    }
}
