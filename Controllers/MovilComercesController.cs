﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using sng_web.Data;
using sng_web.Models;

namespace sng_web.Controllers
{
    [Produces("application/json")]
    [Route("api/MovilComerces")]
    public class MovilComercesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public MovilComercesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/MovilComerces
        [HttpGet]
        public IEnumerable<Comerce> GetComerce()
        {
            return _context.Comerce;
        }

        // GET: api/MovilComerces/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetComerce([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //var comerce = await _context.Comerce.SingleOrDefaultAsync(m => m.ComerceId == id);
            var comerce = await _context.Comerce
                            //.Include(c => c.Category)
                            //.Include(c => c.Owner)
                            .SingleOrDefaultAsync(m => m.ComerceId == id);

            if (comerce == null)
            {
                return NotFound();
            }

            var products = await _context.Product.Where(product => product.Comerce.ComerceId == id).ToListAsync();
            var cupons = await _context.Cupon.Where(cupon => cupon.ComerceId == id).ToListAsync();
            var vacants = await _context.Vacant.Where(vacant => vacant.ComerceId == id).ToListAsync();
            
            if (products.Count > 0)
                comerce.Products = products;

            if(cupons.Count > 0)
                comerce.Cupons = cupons;

            if (vacants.Count > 0)
                comerce.Vacants = vacants;

            return Ok(comerce);
        }

        // PUT: api/MovilComerces/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutComerce([FromRoute] long id, [FromBody] Comerce comerce)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != comerce.ComerceId)
            {
                return BadRequest();
            }

            _context.Entry(comerce).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ComerceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MovilComerces
        [HttpPost]
        public async Task<IActionResult> PostComerce([FromBody] Comerce comerce)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Comerce.Add(comerce);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetComerce", new { id = comerce.ComerceId }, comerce);
        }

        // DELETE: api/MovilComerces/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteComerce([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var comerce = await _context.Comerce.SingleOrDefaultAsync(m => m.ComerceId == id);
            if (comerce == null)
            {
                return NotFound();
            }

            _context.Comerce.Remove(comerce);
            await _context.SaveChangesAsync();

            return Ok(comerce);
        }

        private bool ComerceExists(long id)
        {
            return _context.Comerce.Any(e => e.ComerceId == id);
        }
    }
}