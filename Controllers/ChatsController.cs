using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


using FireSharp.Response;
using Microsoft.AspNetCore.Mvc;
using sng_web.Data;


using Google.Cloud.Firestore;
using System.Security.Claims;
using sng_web.Common;
using System.Collections;
using sng_web.Services;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Firestore.V1;
using Grpc.Core;
using Grpc.Auth;

namespace sng_web.Controllers
{
    public class ChatsController : Controller
    {

        private readonly INotifications _notification;
        String project = "sng-app-112d7";

        public object AuthExplicit(string jsonPath)
        {
            // Explicitly use service account credentials by specifying
            // the private key file.
            var credential = Google.Apis.Auth.OAuth2.GoogleCredential.FromFile(jsonPath);
            return null;
        }
        private readonly ApplicationDbContext _context;

        public ChatsController(ApplicationDbContext context, INotifications notification)
        {
            _context = context;
            _notification = notification;

            String value = Environment.GetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS");
            Console.WriteLine("Variable de GOOGLE_APPLICATION_CREDENTIALS ", value);

            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", "/root/sng_web/keyfile.json");

            value = Environment.GetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS");
            Console.WriteLine("Variable de GOOGLE_APPLICATION_CREDENTIALS ", value);
            // FirestoreDb db = FirestoreDb.Create(project);
            // Console.WriteLine("Created Cloud Firestore client with project ID: {0}", project);
        }

        // GET: Owners
        public async Task<IActionResult> Index()
        {
            string comerceId = HttpContext.Request.Query["comerceId"].ToString();
            ViewData["comerceId"] = comerceId;

            return View();
        }

        public async Task<IActionResult> Details(int? id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            var comerceId = 0;
            var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
            if (comerce_claim != null)
                comerceId = Int32.Parse(comerce_claim.Value);

            if (comerceId != 0)
            {
                var comerce = _context.Comerce.FirstOrDefault(c => c.ComerceId == comerceId);
                return View(comerce);
            }

            return View();
        }

        public async Task<IActionResult> GetChats()
        {
            try
            {
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims;


                string comerceId = HttpContext.Request.Query["comerceId"].ToString();
                FirestoreDb db = this.getFirebasedb();
                CollectionReference chatRef = db.Collection("chatroom");
                QuerySnapshot snapshot;

                if (User.IsInRole("Admin"))
                {
                    if (comerceId == null)
                    {
                        snapshot = await chatRef.GetSnapshotAsync();
                    }
                    else
                    {
                        if (comerceId == "")
                        {
                            var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                            if (comerce_claim != null)
                                comerceId = comerce_claim.Value;
                        }

                        Query query = chatRef.WhereEqualTo("comercioId", Int32.Parse(comerceId));
                        snapshot = await query.GetSnapshotAsync();
                    }
                }
                else // is comerce roles
                {
                    if (comerceId == "")
                    {
                        var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                        if (comerce_claim != null)
                            comerceId = comerce_claim.Value;
                    }
                    if (comerceId == "")
                    {
                        StatusCode(400, new { Message = "Commerce is required" });
                    }
                    Query query = chatRef.WhereEqualTo("comercioId", Int32.Parse(comerceId));
                    snapshot = await query.GetSnapshotAsync();
                }

                Dictionary<string, object>[] matrix = new Dictionary<string, object>[snapshot.Documents.Count];
                var index = 0;
                foreach (DocumentSnapshot document in snapshot.Documents)
                {
                    Console.WriteLine("User: {0}", document.Id);
                    Dictionary<string, object> documentDictionary = document.ToDictionary();

                    var user_id = documentDictionary["clienteId"];
                    if (user_id != null)
                    {
                        var user = _context.ApplicationUser.FirstOrDefault(u => u.Id == user_id.ToString());
                        documentDictionary.Add("id", document.Id);
                        documentDictionary.Add("user", user);
                        matrix[index] = documentDictionary;
                    }
                    index++;
                }

                return StatusCode(200, matrix.ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = "internal error " + ex.Message + " " + ex.StackTrace.ToString() });
            }
        }

        public async Task<IActionResult> SetChatsVisto(String destinatarioId, String comerceId)
        {

            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            FirestoreDb db = this.getFirebasedb();
            DocumentReference documento = null;

            // 1) ENCONTRAR LA CONVERSACION CON EL CLIENTE
            Query chatRef = db.Collection("chatroom").WhereEqualTo("comercioId", Int32.Parse(comerceId)).WhereEqualTo("clienteId", destinatarioId);
            var snapShot = await chatRef.GetSnapshotAsync();
            if (snapShot.Count > 0)
            {
                // SE ENCONTRO EL HILO DE MENSAJES Y SE PROCEDE A ENVIAR EL MENSAJE
                var documentoid = snapShot.Documents[0].Id;
                documento = db.Collection("chatroom").Document(documentoid);

            }
            else
            {
                // NO SE ENCONTRO LA CONVERSACION, CREAR CONVERSACION
                Dictionary<string, object> data = new Dictionary<string, object>
                                            {
                                                { "clienteId", destinatarioId },
                                                { "comercioId", Int32.Parse(comerceId) },
                                                { "mensajes", new ArrayList() },
                                                { "nombreComercio", "" }
                                            };
                documento = await db.Collection("chatroom").AddAsync(data);
            }

            // 2) SETEAR MENSAJES COMO VISTOS
            var datos_actuales = (await documento.GetSnapshotAsync()).ToDictionary();
            List<Object> mensajes = (List<Object>)datos_actuales["mensajes"];
            foreach (Dictionary<string, object> mensaje in mensajes)
            {
                var es_cliente = Convert.ToBoolean(mensaje["client"]);
                if (es_cliente == true)
                {
                    mensaje["visto"] = true;
                }
            }

            await documento.SetAsync(datos_actuales, SetOptions.MergeAll);

            return StatusCode(200, new { Message = "Chat viewed" });
        }

        [HttpPost]
        public async Task<IActionResult> SendChats(String mensaje, String destinatarioId, String comerceId)
        {

            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            FirestoreDb db = this.getFirebasedb();
            DocumentReference documento = null;

            // 1) ENCONTRAR LA CONVERSACION CON EL CLIENTE
            Query chatRef = db.Collection("chatroom").WhereEqualTo("comercioId", Int32.Parse(comerceId)).WhereEqualTo("clienteId", destinatarioId);
            var snapShot = await chatRef.GetSnapshotAsync();
            if (snapShot.Count > 0)
            {
                // SE ENCONTRO EL HILO DE MENSAJES Y SE PROCEDE A ENVIAR EL MENSAJE
                var documentoid = snapShot.Documents[0].Id;
                documento = db.Collection("chatroom").Document(documentoid);

            }
            else
            {
                // NO SE ENCONTRO LA CONVERSACION, CREAR CONVERSACION
                Dictionary<string, object> data = new Dictionary<string, object>
                                            {
                                                { "clienteId", destinatarioId },
                                                { "comercioId", Int32.Parse(comerceId) },
                                                { "mensajes", new ArrayList() },
                                                { "nombreComercio", "" }
                                            };
                documento = await db.Collection("chatroom").AddAsync(data);
            }

            // 2) AGREGAR MENSAJE A LA CONVERSACIÓN
            var datos_actuales = (await documento.GetSnapshotAsync()).ToDictionary();
            List<Object> mensajes = (List<Object>)datos_actuales["mensajes"];
            var avatar = _context.Comerce.FirstOrDefault(c => c.ComerceId == Int32.Parse(comerceId)).UrlLogo;
            var nombre = _context.Comerce.FirstOrDefault(c => c.ComerceId == Int32.Parse(comerceId)).Name;

            mensajes.Add(new Dictionary<string, object>
                                            {
                                                { "fecha",  DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") },
                                                { "mensaje", mensaje },
                                                { "userId", Int32.Parse(comerceId) },
                                                { "avatar", avatar},
                                                { "nombre", nombre},
                                                { "visto", false},
                                                { "client", false}
                                            });

            await documento.SetAsync(datos_actuales, SetOptions.MergeAll);

            Query oneSignalRef = db.Collection("onesingal_ids").WhereEqualTo("userId", destinatarioId);
            snapShot = await oneSignalRef.GetSnapshotAsync();
            if (snapShot.Count > 0)
            {
                datos_actuales = snapShot.Documents[0].ToDictionary();
                List<String> onSignalIds = new List<string>();
                foreach (var id in (List<Object>)datos_actuales["ids"])
                {
                    onSignalIds.Add(id.ToString());
                }

                var data_notificacion = new Dictionary<string, string>(){
                    {"Model", "mensaje"},
                    {"Mensaje", mensaje},
                    {"ComerceId", comerceId}
                };
                try
                {
                    await _notification.CreateNotification("¡Hay un nuevo mensaje para ti!", data_notificacion, onSignalIds);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }



            return StatusCode(200, new { Message = "Chat registered" });
        }


        public IActionResult Test()
        {
            return View();
        }

        private FirestoreDb getFirebasedb()
        {
            GoogleCredential cred = GoogleCredential.FromFile("keyfile.json");
            Channel channel = new Channel(
                FirestoreClient.DefaultEndpoint.Host,
                FirestoreClient.DefaultEndpoint.Port,
                cred.ToChannelCredentials());
            Console.WriteLine("Credenciales desde archivo " + channel.Target + channel.State);
            FirestoreClient client = FirestoreClient.Create(channel);
            //var storage = StorageClient.Create(credential);

            FirestoreDb db = FirestoreDb.Create(project, client);
            return db;
        }
    }
}
