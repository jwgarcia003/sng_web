﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OneSignal.CSharp.SDK;
using OneSignal.CSharp.SDK.Resources;
using OneSignal.CSharp.SDK.Resources.Notifications;
using sng_web.Common;
using sng_web.Data;
using sng_web.Models;
using sng_web.Services;

namespace sng_web.Controllers
{
    public class ProductsController : Controller
    {
        private readonly INotifications _notification;
        private readonly ApplicationDbContext _context;
        //Upload Images to server
        private readonly IHostingEnvironment _iHostingEnvironment;

        public ProductsController(ApplicationDbContext context, 
            IHostingEnvironment env,
            INotifications notification)
        {
            _context = context;
            _iHostingEnvironment = env;
            _notification = notification;
        }

        // POST: add new product 
        // Ajax method
        public async Task<List<IdentityError>> AddProduct(string Name, string Description, Double Price, string Currency, string UrlImage, int ComerceId)
        {

            var comerce = await _context.Comerce.SingleOrDefaultAsync(m => m.ComerceId == ComerceId);

            List<IdentityError> errors = new List<IdentityError>();
            Product product = new Product()
            {
                Name = Name,
                Description = Description,
                Price = Price,
                Currency = Currency,
                UrlImage = UrlImage,
                Comerce = comerce,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                IsActived = true
            };

            var file = HttpContext.Request.Form.Files.FirstOrDefault(f => f.Name == "UrlImage");
            if ((file != null && file.Length > 0) || product.UrlImage == null)
                product.UrlImage = await Utilities.upload_fileAsync(HttpContext, "images/uploads/products/", "/images/product.jpg");
            _context.Add(product);
            await _context.SaveChangesAsync();

            if(product.IsActived && product.Comerce.IsAproved){
                var data = new Dictionary<string,string>(){
                    {"Model", "product"},
                    {"ObjectId", product.ProductId.ToString()},
                    {"ComerceId", product.ComerceId.ToString()}
                };
                await _notification.CreateNotification("¡Tenemos un nuevo producto que te insterezará!", data);
            }
            


            errors.Add(new IdentityError()
            {
                Code = "Saved",
                Description = "Producto guardado exitosamente."
            });
            return errors;
        }

        // PUT: update product 
        // Ajax method
        public async Task<List<IdentityError>> UpdateProduct(int ProductId, string Name, string Description, Double Price, string UrlImage, 
            DateTime CreatedAt, bool IsActived, int ComerceId)
        {
            var comerce = await _context.Comerce.SingleOrDefaultAsync(m => m.ComerceId == ComerceId);

            List<IdentityError> errors = new List<IdentityError>();
            Product product = new Product()
            {
                ProductId = ProductId,
                Name = Name,
                Description = Description,
                Price = Price,
                UrlImage = UrlImage,
                Comerce = comerce,
                CreatedAt = CreatedAt,
                UpdatedAt = DateTime.Now,
                IsActived = IsActived
            };
            _context.Update(product);
            await _context.SaveChangesAsync();

            errors.Add(new IdentityError()
            {
                Code = "Updated",
                Description = "El producto "+ Name  + " ha sido actualizado exitosamente."
            });
            return errors;
        }

        // DELETE: delete product 
        // Ajax method
        public async Task<List<IdentityError>> DeleteProduct(int id, string name)
        {
            List<IdentityError> errors = new List<IdentityError>();
            var product = await _context.Product.SingleOrDefaultAsync(m => m.ProductId == id);

            if (product == null)
            {
                errors.Add(new IdentityError()
                {
                    Code = "Error",
                    Description = "El producto "+ name + " no ha sido encontrado."
                });
                return  errors;
            }

            _context.Product.Remove(product);
            await _context.SaveChangesAsync();

            errors.Add(new IdentityError()
            {
                Code = "Deleted",
                Description = "El producto "+ name +" ha sido eliminado."
            });

            return errors;
        }

        // GET: Get all product registers
        // Ajax method
        public async Task<ICollection<Product>> GetProducts()
        {
            
            TempData["url_comerce_details"] = null;
            var products = await _context.Product.Include(c => c.Comerce)
                                                    .OrderBy(p => p.ComerceId)
                                                .ToListAsync();
            List<Product> list = new List<Product>();
            foreach (var p in products)
            {
                p.Comerce = await _context.Comerce.FirstOrDefaultAsync(c => c.ComerceId == p.ComerceId);
                list.Add(p);
            }
            list.OrderBy(p => p.ComerceId);
            return (list);
        }

        public async Task<ICollection<Product>> GetProductsByComerce(int ComerceId, int skip, int take, string orderBy)
        {
            var products = await _context.Product.Where(c => c.Comerce.ComerceId == ComerceId)
                                                    .Include(c=>c.Comerce)
                                                    .OrderBy(p => p.ProductId)
                                                    .ToListAsync();
            List<Product> list = new List<Product>();
            foreach(var p in products)
            {
                //p.Comerce = await _context.Comerce.FirstOrDefaultAsync(c => c == p.Comerce);
                list.Add(p);
            }

            return ( list);
        }
        //   Action methods  :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


        //Metodo de prueba para subir fotos
        [HttpPost]
        public async Task<IActionResult> UploadFilesAjax(Product p, List<IFormFile> files)
        {
            var comerce = await _context.Comerce.SingleOrDefaultAsync(m => m.ComerceId == p.Comerce.ComerceId);

            List<IdentityError> errors = new List<IdentityError>();
            Product product = new Product()
            {
                Name = p.Name,
                Description = p.Description,
                Price = p.Price,
                Comerce = comerce,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                IsActived = true
            };
            
            //var files = HttpContext.Request.Form.Files;
            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    if (file != null && file.Length > 0)
                    {
                        var uploads = Path.Combine(_iHostingEnvironment.WebRootPath, "images/uploads/products/");
                        var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                        using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                        {
                            await file.CopyToAsync(fileStream);
                            p.UrlImage = "/images/uploads/products/" + fileName;
                        }
                    }
                }else
                {
                    if (p.UrlImage == null)
                        p.UrlImage = "/images/product.jpg";
                }
            }

            _context.Add(product);
            await _context.SaveChangesAsync();

            errors.Add(new IdentityError()
            {
                Code = "Saved",
                Description = "Producto guardado exitosamente."
            });
            return null;
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Product.ToListAsync());
        }

        // GET: Products/Details/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Product
                    .Include(p => p.Comerce)
                .SingleOrDefaultAsync(m => m.ProductId == id);
            if (product == null)
            {
                return NotFound();
            }

            var commerce = await _context.Comerce.Where(comerce => comerce == product.Comerce)
                .ToListAsync();
            //product.Comerce = commerce;

            return View(product);
        }

        // GET: Products/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            ViewData["ComerceId"] = new SelectList(_context.Comerce, "ComerceId", "Name");
            ViewData["Currency"] = new SelectListItem[] {
                new SelectListItem() { Text = "C$", Value = "C$" },
                new SelectListItem() { Text = "$", Value = "$" },
            };
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("ProductId,Name,Description,Price,Currency,UrlImage,CreatedAt,UpdatedAt,IsActived,ComerceId")] Product product)
        {
            var comerce = await _context.Comerce.SingleOrDefaultAsync(m => m.ComerceId == product.ComerceId);
            if (ModelState.IsValid)
            {
                var file =  HttpContext.Request.Form.Files.FirstOrDefault(f => f.Name == "UrlImage");
                if ((file != null && file.Length > 0) || product.UrlImage==null)
                    product.UrlImage = await Utilities.upload_fileAsync(HttpContext, "images/uploads/products/", "/images/product.jpg");
                //await UploadImage(product);
                product.CreatedAt = DateTime.Now;
                product.UpdatedAt = DateTime.Now;
                product.Comerce = comerce;
                _context.Add(product);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Edit/5
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var product = await _context.Product.SingleOrDefaultAsync(m => m.ProductId == id);
            if (product == null)
                return NotFound();

            //Validate authentication from user
            ViewBag.Refer = Request.Headers["Referer"].ToString();
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (!User.IsInRole("Admin"))
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                if (comerce_claim == null || Convert.ToInt32(comerce_claim.Value) != product.ComerceId)
                    return NotFound();
            }

            ViewData["ComerceId"] = new SelectList(_context.Comerce, "ComerceId", "Name");
            ViewData["Currency"] = new SelectListItem[] {
                new SelectListItem() { Text = "C$", Value = "C$" },
                new SelectListItem() { Text = "$", Value = "$" },
            };

            ViewBag.CreatedAt = product.CreatedAt;
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Edit(int id, [Bind("ProductId,Name,Description,Price,Currency,UrlImage,CreatedAt,UpdatedAt,IsActived,ComerceId")] Product product)
        {
            if (id != product.ProductId)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    //var p = await _context.Product.SingleOrDefaultAsync(m => m.ProductId == id);
                    //product.UpdatedAt = DateTime.Now;
                    //if (Request.Form["CreatedAt"].ToString() != "")
                    //    product.CreatedAt = DateTime.Parse(Request.Form["CreatedAt"].ToString());
                    //else product.CreatedAt = DateTime.Now;
                    product.UpdatedAt = DateTime.Now;
                    var file = HttpContext.Request.Form.Files.FirstOrDefault(f => f.Name == "UrlImage");
                    if ((file != null && file.Length > 0) || product.UrlImage==null)
                        product.UrlImage = await Utilities.upload_fileAsync(HttpContext, "images/uploads/products/", "/images/product.jpg");
                    _context.Update(product);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(product.ProductId))
                        return NotFound();
                    else
                        throw;
                }

                if (Request.Form["Refer"].ToString() != "")
                    return Redirect(Request.Form["Refer"].ToString());
                else
                    return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Delete/5
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var product = await _context.Product.SingleOrDefaultAsync(m => m.ProductId == id);
            if (product == null)
                return NotFound();

            var comerce = await _context.Comerce.FirstOrDefaultAsync(c => c.ComerceId == product.ComerceId);
            if(comerce!=null)
                product.Comerce = comerce;

            //Validate authentication from user
            ViewBag.Refer = Request.Headers["Referer"].ToString();
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (!User.IsInRole("Admin"))
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                if (comerce_claim == null || Convert.ToInt32(comerce_claim.Value) != product.ComerceId)
                    return NotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var product = await _context.Product.SingleOrDefaultAsync(m => m.ProductId == id);
            _context.Product.Remove(product);
            await _context.SaveChangesAsync();

            if (Request.Form["Refer"].ToString() != "")
                return Redirect(Request.Form["Refer"].ToString());
            else
                return RedirectToAction(nameof(Index));

        }

        private bool ProductExists(long id)
        {
            return _context.Product.Any(e => e.ProductId == id);
        }
    }
}
