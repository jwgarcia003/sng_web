﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sng_web.Common;
using sng_web.Data;
using sng_web.Models;
using sng_web.Services;
namespace sng_web.Controllers
{
    public class RoyaltiesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INotifications _notification;

        public RoyaltiesController(ApplicationDbContext context, INotifications notification)
        {
            _context = context;
            _notification = notification;
        }

        // POST: add new product 
        // Ajax method
        public async Task<List<IdentityError>> AddRoyalty(string Name, string Description, string Concept, int Count, Double Value, int ComerceId)
        {
            List<IdentityError> errors = new List<IdentityError>();
            Royalty royalty = new Royalty()
            {
                Name = Name,
                Description = Description,
                Concept = Concept,
                Count = Count,
                Value = Value,
                ComerceId = ComerceId,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                IsActive = true
            };
            _context.Add(royalty);
            await _context.SaveChangesAsync();

            if(royalty.IsActive && royalty.Comerce.IsAproved){
                var data = new Dictionary<string,string>(){
                    {"Model", "royalti"},
                    {"ObjectId", royalty.RoyaltyId.ToString()},
                    {"ComerceId", royalty.ComerceId.ToString()}
                };
                await _notification.CreateNotification("¡Nueva regalia disponible por tu compra!", data);
            }
            
            errors.Add(new IdentityError()
            {
                Code = "Saved",
                Description = "¡Regalia guardada exitosamente!"
            });
            return errors;
        }

        public async Task<ICollection<Royalty>> GetRoyalties()
        {
            TempData["url_comerce_details"] = null;
            var royalties = await _context.Royalty
                            .OrderBy(p => p.ComerceId)
                            .ToListAsync();
            List<Royalty> royaltyList = new List<Royalty>();
            foreach (var r in royalties)
            {
                var comerce = await _context.Comerce.SingleOrDefaultAsync(m => m.ComerceId == r.ComerceId);
                if (comerce != null)
                {
                    r.Comerce = comerce;
                }
                royaltyList.Add(r);
            }
            return ( royaltyList);
        }

        public async Task<ICollection<Royalty>> GetRoyaltiesByComerce(int ComerceId, int skip, int take, string orderBy)
            {
                var applicationDbContext = _context.Royalty.Where(c => c.ComerceId == ComerceId)
                                                        //.Skip(skip)
                                                        //.Take(take)
                                                        .OrderBy(p => p.ComerceId)
                                                        .ToListAsync();
                return (await applicationDbContext);
            }

        // GET: Royalties
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            var royalties = await _context.Royalty.ToListAsync();
            return View(royalties);
        }

        // GET: Royalties/Details/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var royalty = await _context.Royalty
                .SingleOrDefaultAsync(m => m.RoyaltyId == id);
            if (royalty == null)
                return NotFound();

            return View(royalty);
        }

        // GET: Royalties/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            ViewData["ComerceId"] = new SelectList(_context.Comerce, "ComerceId", "Name");
            return View();
        }
        
        // POST: Royalties/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("RoyaltyId,Name,Description,Concept,Value,Count,CreatedAt,UpdatedAt,IsActive,Delivered,ComerceId")] Royalty royalty)
        {
            royalty.CreatedAt = DateTime.Now;
            royalty.UpdatedAt = DateTime.Now;
            if (ModelState.IsValid)
            {
                var comerce = await _context.Comerce.SingleOrDefaultAsync(m => m.ComerceId == royalty.ComerceId);
                royalty.Comerce = comerce;
                _context.Add(royalty);
                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            return View(royalty);
        }

        // GET: Royalties/Edit/5
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var royalty = await _context.Royalty.SingleOrDefaultAsync(m => m.RoyaltyId == id);
            if (royalty == null)
                return NotFound();

            //Validate authentication from user
            ViewBag.Refer = Request.Headers["Referer"].ToString();
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (!User.IsInRole("Admin"))
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                if (comerce_claim == null || Convert.ToInt32(comerce_claim.Value) != royalty.ComerceId)
                    return NotFound();
            }

            ViewData["ComerceId"] = new SelectList(_context.Comerce, "ComerceId", "Name");
            return View(royalty);
        }

        // POST: Royalties/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Edit(int id, [Bind("RoyaltyId,Name,Description,Concept,Value,Count,CreatedAt,UpdatedAt,IsActive,Delivered,ComerceId")] Royalty royalty)
        {
            if (id != royalty.RoyaltyId)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(royalty);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RoyaltyExists(royalty.RoyaltyId))
                        return NotFound();
                    else
                        throw;
                }

                if (Request.Form["Refer"].ToString() != "")
                    return Redirect(Request.Form["Refer"].ToString());
                else
                    return RedirectToAction(nameof(Index));
            }
            return View(royalty);
        }

        // GET: Royalties/Delete/5
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var royalty = await _context.Royalty.SingleOrDefaultAsync(m => m.RoyaltyId == id);
            if (royalty == null)
                return NotFound();

            var comerce = await _context.Comerce.SingleOrDefaultAsync(m => m.ComerceId == royalty.ComerceId);
            if (comerce != null)
                royalty.Comerce = comerce;

            //Validate authentication from user
            ViewBag.Refer = Request.Headers["Referer"].ToString();
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (!User.IsInRole("Admin"))
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                if (comerce_claim == null || Convert.ToInt32(comerce_claim.Value) != royalty.ComerceId)
                    return NotFound();
            }

            return View(royalty);
        }

        // POST: Royalties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var royalty = await _context.Royalty.SingleOrDefaultAsync(m => m.RoyaltyId == id);
            _context.Royalty.Remove(royalty);
            await _context.SaveChangesAsync();

            if (Request.Form["Refer"].ToString() != "")
                return Redirect(Request.Form["Refer"].ToString());
            else
                return RedirectToAction(nameof(Index));
        }

        private bool RoyaltyExists(int id)
        {
            return _context.Royalty.Any(e => e.RoyaltyId == id);
        }
    }
}
