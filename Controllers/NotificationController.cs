using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OneSignal.CSharp.SDK;
using OneSignal.CSharp.SDK.Resources;
using OneSignal.CSharp.SDK.Resources.Notifications;
using sng_web.Data;
using sng_web.Models;
using sng_web.Services;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;
using sng_web.Common;
using System.Security.Claims;
using static sng_web.Models.ApplicationLog;
using sng_web.Models.ViewModels;

namespace sng_web.Controllers
{
    public class NotificationController : Controller
    {

        private readonly INotifications _notification;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly INotifications _notifications;
        private readonly ILogger _logger;

        private readonly RoleManager<IdentityRole> _roleManager;

        public static string ROLE_ADMIN = "Admin";
        public static string ROLE_COMERCE = "Comerce";
        public static string ROLE_CLIENT = "Client";



        private readonly ApplicationDbContext _context;

        //Upload Images to server
        private readonly IHostingEnvironment _iHostingEnvironment;

        public NotificationController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            INotifications notification,
            IEmailSender emailSender,
            ILogger<AccountController> logger,
            IHostingEnvironment env,
            ApplicationDbContext context,
            RoleManager<IdentityRole> roleManager)
        {

            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _roleManager = roleManager;

            _context = context;
            _notification = notification;

            _iHostingEnvironment = env;
        }


         // GET: About
        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> Index() 
        {
            return View(await _context.Notifications.ToListAsync());
        }

        // GET: About/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var about = await _context.About
                .SingleOrDefaultAsync( a => a.AboutId == id);
            if (about == null)
            {
                return NotFound();
            }

            return View(about);
        }

        // GET: abouts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: abouts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("NotificationId,Tittle,Message,Model,ModelReferenceId")] Notification notif, String Comercio)
        {
            if (ModelState.IsValid)
            {
                notif.CreatedAt = DateTime.Now;
                
                if (notif.Model == "Comercio") {
                    var comerce = await _context.Comerce.SingleOrDefaultAsync(m => m.ComerceId == Int32.Parse(Comercio));
                    var data = new Dictionary<string,string>(){
                            {"Model", "comerce"},
                            {"ObjectId", comerce.ComerceId.ToString()},
                            {"ComerceId", comerce.ComerceId.ToString()}
                        };
                    await _notification.CreateNotification(notif.Tittle, notif.Message, data);
                } else if (notif.Model == "Producto") {
                    var product = await _context.Product.SingleOrDefaultAsync(m => m.ProductId == Int32.Parse(notif.ModelReferenceId));
                    var data = new Dictionary<string,string>(){
                        {"Model", "product"},
                        {"ObjectId", product.ProductId.ToString()},
                        {"ComerceId", product.ComerceId.ToString()}
                    };
                    await _notification.CreateNotification(notif.Tittle, notif.Message, data);
                } else if (notif.Model == "Promoción") {
                    var promotion = await _context.Promotion.SingleOrDefaultAsync(m => m.PromotionId == Int32.Parse(notif.ModelReferenceId));
                    var data = new Dictionary<string,string>(){
                        {"Model", "promotion"},
                        {"ObjectId", promotion.PromotionId.ToString()},
                        {"ComerceId", promotion.ComerceId.ToString()}
                    };
                    await _notification.CreateNotification(notif.Tittle, notif.Message, data);
                } else {
                    await _notification.CreateNotification(notif.Tittle, notif.Message);
                }

                notif.SentAt = DateTime.Now;
                _context.Add(notif);
                await _context.SaveChangesAsync();
                

                

                return RedirectToAction(nameof(Index));
            }
            return View(notif);
        }

        // GET: abouts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var about = await _context.About.SingleOrDefaultAsync( a => a.AboutId == id);
            if (about == null)
            {
                return NotFound();
            }
            return View(about);
        }

        // POST: abouts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AboutId,Name,Description,Address,Phone, PhoneSales, ContactEmail, SuportPage, TermsPrivacy,Exchanges")] About about)
        {
            if (id != about.AboutId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(about);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!aboutExists(about.AboutId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(about);
        }

        // // GET: Owners/Delete/5
        // public async Task<IActionResult> Delete(int? id)
        // {
        //     if (id == null)
        //     {
        //         return NotFound();
        //     }

        //     var owner = await _context.Owner
        //         .SingleOrDefaultAsync(m => m.OwnerId == id);
        //     if (owner == null)
        //     {
        //         return NotFound();
        //     }

        //     return View(owner);
        // }

        // // POST: Owners/Delete/5
        // [HttpPost, ActionName("Delete")]
        // [ValidateAntiForgeryToken]
        // public async Task<IActionResult> DeleteConfirmed(int id)
        // {
        //     var owner = await _context.Owner.SingleOrDefaultAsync(m => m.OwnerId == id);
        //     _context.Owner.Remove(owner);
        //     await _context.SaveChangesAsync();
        //     return RedirectToAction(nameof(Index));
        // }

        private bool aboutExists(int id)
        {
            return _context.About.Any(e => e.AboutId == id);
        }

    }
}
