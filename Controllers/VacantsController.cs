﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OneSignal.CSharp.SDK;
using OneSignal.CSharp.SDK.Resources;
using OneSignal.CSharp.SDK.Resources.Notifications;
using sng_web.Common;
using sng_web.Data;
using sng_web.Models;
using sng_web.Services;

namespace sng_web.Controllers
{
    public class VacantsController : Controller
    {
        private readonly INotifications _notification;
        private readonly ApplicationDbContext _context;

        public VacantsController(ApplicationDbContext context, INotifications notification)
        {
            _context = context;
            _notification = notification;
        }

        // POST: add new product
        // Ajax method
        public async Task<List<IdentityError>> AddVacant(string Name, string Description, string Area, string Cargo, 
            int AvailableVacants, string Schedule, string Experience, string Genre, 
            int Age, Double MaxSalary, Double MinSalary, string Department, String Country, int ComerceId)
        {
            List<IdentityError> errors = new List<IdentityError>();
            var comerce = await _context.Comerce.SingleOrDefaultAsync(m => m.ComerceId == ComerceId);
            Vacant vacant = new Vacant()
            {
               Name = Name,
               Description = Description,
               Area = Area,
               Cargo = Cargo,
               AvailableVacants = AvailableVacants,
               schedule = Schedule,
               Experience = Experience,
               Genre = Genre,
               Age = Age,
               MaxSalary = MaxSalary,
               MinSalary = MinSalary,
               Department = Department,
               Country = Country,
               IsActive = true,
               ComerceId = ComerceId,
               Comerce = comerce
            };
            _context.Add(vacant);
            await _context.SaveChangesAsync();

            if(vacant.IsActive && comerce.IsAproved){
                var data = new Dictionary<string,string>(){
                    {"Model", "vacant"},
                    {"ObjectId", vacant.VacantId.ToString()},
                    {"ComerceId", comerce.ComerceId.ToString()}
                };
                await _notification.CreateNotification("¡Nueva Vacante disponible, aplica ahora mismo!", data);
            }
            
            errors.Add(new IdentityError()
            {
                Code = "Saved",
                Description = "Vacante guardada exitosamente."
            });
            return errors;
        }

        public async Task<ICollection<Vacant>> GetVacants()
        {
            var vacants = await _context.Vacant.Include(v => v.Comerce)
                                                .ToListAsync();

            List<Vacant> list = new List<Vacant>();
            foreach (var v in vacants)
            {
                v.Comerce = await _context.Comerce.FirstOrDefaultAsync(c => c.ComerceId == v.ComerceId);
                list.Add(v);
            }

            return (list);
        }

        public async Task<ICollection<Vacant>> GetVacantsByComerce(int ComerceId, int skip, int take, string orderby)
        {
            var vacants = await _context.Vacant.Where(v => v.ComerceId == ComerceId)
                                                    .Include(v => v.Comerce)
                                                    .Skip(skip)
                                                    .Take(take)
                                                    //.OrderBy(p => orderby)
                                                    .ToListAsync();

            List<Vacant> list = new List<Vacant>();
            foreach (var v in vacants)
            {
                v.Comerce = await _context.Comerce.FirstOrDefaultAsync(c => c.ComerceId == ComerceId);
                list.Add(v);
            }

            return (list);
        }

        public async Task<ICollection<Vacant>> GetVacantsApliedByComerce(int ComerceId, int skip, int take, string orderby)
        {
            var applicationDbContext = _context.Vacant.Where(v => v.ComerceId == ComerceId)
                                                    .Skip(skip)
                                                    .Take(take)
                                                    //.OrderBy(p => orderby)
                                                    .ToListAsync();
            return (await applicationDbContext);
        }

        // GET: Vacants
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Vacant.ToListAsync());
        }

        // GET: Vacants/Details/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vacant = await _context.Vacant
                .SingleOrDefaultAsync(m => m.VacantId == id);
            if (vacant == null)
            {
                return NotFound();
            }

            return View(vacant);
        }

        // GET: Vacants/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Vacants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("VacantId,Name,Description,Area,Cargo,AvailableVacants,schedule,Experience,Genre,Age,MaxSalary,MinSalary,Department,Country,IsActive,ComerceId")] Vacant vacant)
        {
            if (ModelState.IsValid)
            {
                _context.Add(vacant);
                await _context.SaveChangesAsync();
               
                return RedirectToAction(nameof(Index));
            }
            return View(vacant);
        }

        // GET: Vacants/Edit/5
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var vacant = await _context.Vacant.SingleOrDefaultAsync(m => m.VacantId == id);
            if (vacant == null)
                return NotFound();

            //Validate authentication from user
            ViewBag.Refer = Request.Headers["Referer"].ToString();
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (!User.IsInRole("Admin"))
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                if (comerce_claim == null || Convert.ToInt32(comerce_claim.Value) != vacant.ComerceId)
                    return NotFound();
            }

            return View(vacant);
        }

        // POST: Vacants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Edit(int id, [Bind("VacantId,Name,Description,Area,Cargo,AvailableVacants,schedule,Experience,Genre,Age,MaxSalary,MinSalary,Department,Country,IsActive,ComerceId")] Vacant vacant)
        {
            if (id != vacant.VacantId)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(vacant);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VacantExists(vacant.VacantId))
                        return NotFound();
                    else
                        throw;
                }

                if (Request.Form["Refer"].ToString() != "")
                    return Redirect(Request.Form["Refer"].ToString());
                else
                    return RedirectToAction(nameof(Index));
            }
            return View(vacant);
        }

        // GET: Vacants/Delete/5
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var vacant = await _context.Vacant.SingleOrDefaultAsync(m => m.VacantId == id);
            if (vacant == null)
                return NotFound();

            //Validate authentication from user
            ViewBag.Refer = Request.Headers["Referer"].ToString();
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (!User.IsInRole("Admin"))
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                if (comerce_claim == null || Convert.ToInt32(comerce_claim.Value) != vacant.ComerceId)
                    return NotFound();
            }

            return View(vacant);
        }

        // POST: Vacants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var vacant = await _context.Vacant.SingleOrDefaultAsync(m => m.VacantId == id);
            _context.Vacant.Remove(vacant);
            await _context.SaveChangesAsync();

            if (Request.Form["Refer"].ToString() != "")
                return Redirect(Request.Form["Refer"].ToString());
            else
                return RedirectToAction(nameof(Index));
        }

        private bool VacantExists(int id)
        {
            return _context.Vacant.Any(e => e.VacantId == id);
        }
    }
}
