﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace sng_web.Common
{
    public static class Utilities
    {
        public static async Task<string> upload_fileAsync(
            HttpContext httpContext, 
            string path_to_save, 
            string paht_altern=null)
        {
            string ruta_archivo = null;
            var files = httpContext.Request.Form.Files;
            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    if (file != null && file.Length > 0)
                    {
                        var uploads = Path.Combine(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot"), path_to_save);
                        if (!Directory.Exists(uploads))
                            Directory.CreateDirectory(uploads);
                        var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                        using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                        {
                            await file.CopyToAsync(fileStream);
                            ruta_archivo = "/" + path_to_save + fileName;
                        }

                    }
                }
                else
                {
                    if(paht_altern!=null)
                        ruta_archivo = paht_altern;
                }
            }

            return ruta_archivo;
        }
    }
}
