using Google.Apis.Auth.OAuth2;
using Google.Cloud.Firestore;
using Google.Cloud.Firestore.V1;
using Grpc.Auth;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sng_web.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {
        String project = "sng-app-112d7";

        private readonly static ConnectionMapping<string> _connections = 
            new ConnectionMapping<string>();

        IHubContext<ChatHub> _hubContext = null;
        
        public ChatHub(IHubContext<ChatHub> hubContext)
        {
            _hubContext = hubContext;
        }
       public override Task OnConnectedAsync()
        {
            string name = Context.User.Identity.Name;

            _connections.Add(name, Context.ConnectionId);
            
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            string name = Context.User.Identity.Name;

            _connections.Remove(name, Context.ConnectionId);

            return base.OnDisconnectedAsync(exception);
        }


        public async Task GetMessages(string chatroom)
        {
            try {
                FirestoreDb db = this.getFirebasedb();
                DocumentReference docRef = db.Collection("chatroom").Document(chatroom);
                FirestoreChangeListener listener = docRef.Listen(async snapshot =>
                {
                    Console.WriteLine("Callback received document snapshot.");
                    Console.WriteLine("Document exists? {0}", snapshot.Exists);
                    if (snapshot.Exists)
                    {
                        Console.WriteLine("Document data for {0} document:", snapshot.Id);
                        Dictionary<string, object> data = snapshot.ToDictionary();
                        foreach (KeyValuePair<string, object> pair in data)
                        {
                            Console.WriteLine("{0}: {1}", pair.Key, pair.Value);
                        }
                        await _hubContext.Clients.All.SendAsync("ReceiveMessage", data);
                    }
                    else
                    {
                        Console.WriteLine("Document {0} does not exist!", snapshot.Id);
                        await _hubContext.Clients.All.SendAsync("ReceiveMessage", "log-info: Chat no encontrado");
                    }
                });
                await Clients.Caller.SendAsync("ReceiveMessage", "log-info: Conexión iniciada");
            }catch(Exception e) {
                await Clients.Caller.SendAsync("ReceiveMessage", "log-info: " + e.Message);
            }
            
        }
    
        private FirestoreDb getFirebasedb(){
            GoogleCredential cred = GoogleCredential.FromFile("keyfile.json");
            Channel channel = new Channel(
                FirestoreClient.DefaultEndpoint.Host,
                FirestoreClient.DefaultEndpoint.Port,
                cred.ToChannelCredentials());
            Console.WriteLine("Credenciales desde archivo " + channel.Target + channel.State);
            FirestoreClient client = FirestoreClient.Create(channel);
            //var storage = StorageClient.Create(credential);
            
            FirestoreDb db = FirestoreDb.Create(project, client);
            return db;
        }
    }


    public class ConnectionMapping<T>
    {
        private readonly Dictionary<T, HashSet<string>> _connections =
            new Dictionary<T, HashSet<string>>();

        public int Count
        {
            get
            {
                return _connections.Count;
            }
        }

        public void Add(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    connections = new HashSet<string>();
                    _connections.Add(key, connections);
                }

                lock (connections)
                {
                    connections.Add(connectionId);
                }
            }
        }

        public IEnumerable<string> GetConnections(T key)
        {
            HashSet<string> connections;
            if (_connections.TryGetValue(key, out connections))
            {
                return connections;
            }

            return Enumerable.Empty<string>();
        }

        public void Remove(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    return;
                }

                lock (connections)
                {
                    connections.Remove(connectionId);

                    if (connections.Count == 0)
                    {
                        _connections.Remove(key);
                    }
                }
            }
        }
    }
}