﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace sng_web.Migrations
{
    public partial class Notifications : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserMessageComment_Comerce_ComerceId1",
                table: "UserMessageComment");

            migrationBuilder.DropForeignKey(
                name: "FK_UserMessageComment_AspNetUsers_applicationUserId",
                table: "UserMessageComment");

            migrationBuilder.DropIndex(
                name: "IX_UserMessageComment_ComerceId1",
                table: "UserMessageComment");

            migrationBuilder.DropIndex(
                name: "IX_UserMessageComment_applicationUserId",
                table: "UserMessageComment");

            migrationBuilder.DropColumn(
                name: "ComerceId1",
                table: "UserMessageComment");

            migrationBuilder.DropColumn(
                name: "applicationUserId",
                table: "UserMessageComment");

            migrationBuilder.AlterColumn<long>(
                name: "ComerceId",
                table: "UserMessageComment",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Notifications",
                columns: table => new
                {
                    NotificationId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    Message = table.Column<string>(nullable: false),
                    Model = table.Column<string>(nullable: true),
                    ModelReferenceId = table.Column<string>(nullable: true),
                    SentAt = table.Column<DateTime>(nullable: false),
                    Tittle = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifications", x => x.NotificationId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserMessageComment_ComerceId",
                table: "UserMessageComment",
                column: "ComerceId");

            migrationBuilder.CreateIndex(
                name: "IX_UserMessageComment_UserId",
                table: "UserMessageComment",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserMessageComment_Comerce_ComerceId",
                table: "UserMessageComment",
                column: "ComerceId",
                principalTable: "Comerce",
                principalColumn: "ComerceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserMessageComment_AspNetUsers_UserId",
                table: "UserMessageComment",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserMessageComment_Comerce_ComerceId",
                table: "UserMessageComment");

            migrationBuilder.DropForeignKey(
                name: "FK_UserMessageComment_AspNetUsers_UserId",
                table: "UserMessageComment");

            migrationBuilder.DropTable(
                name: "Notifications");

            migrationBuilder.DropIndex(
                name: "IX_UserMessageComment_ComerceId",
                table: "UserMessageComment");

            migrationBuilder.DropIndex(
                name: "IX_UserMessageComment_UserId",
                table: "UserMessageComment");

            migrationBuilder.AlterColumn<int>(
                name: "ComerceId",
                table: "UserMessageComment",
                nullable: true,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ComerceId1",
                table: "UserMessageComment",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "applicationUserId",
                table: "UserMessageComment",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserMessageComment_ComerceId1",
                table: "UserMessageComment",
                column: "ComerceId1");

            migrationBuilder.CreateIndex(
                name: "IX_UserMessageComment_applicationUserId",
                table: "UserMessageComment",
                column: "applicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserMessageComment_Comerce_ComerceId1",
                table: "UserMessageComment",
                column: "ComerceId1",
                principalTable: "Comerce",
                principalColumn: "ComerceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserMessageComment_AspNetUsers_applicationUserId",
                table: "UserMessageComment",
                column: "applicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
