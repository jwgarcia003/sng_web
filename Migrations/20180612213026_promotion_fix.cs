﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace sng_web.Migrations
{
    public partial class promotion_fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Promotion_Comerce_ComerceId1",
                table: "Promotion");

            migrationBuilder.DropIndex(
                name: "IX_Promotion_ComerceId1",
                table: "Promotion");

            migrationBuilder.DropColumn(
                name: "ComerceId1",
                table: "Promotion");

            migrationBuilder.AlterColumn<long>(
                name: "ComerceId",
                table: "Promotion",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_Promotion_ComerceId",
                table: "Promotion",
                column: "ComerceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Promotion_Comerce_ComerceId",
                table: "Promotion",
                column: "ComerceId",
                principalTable: "Comerce",
                principalColumn: "ComerceId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Promotion_Comerce_ComerceId",
                table: "Promotion");

            migrationBuilder.DropIndex(
                name: "IX_Promotion_ComerceId",
                table: "Promotion");

            migrationBuilder.AlterColumn<int>(
                name: "ComerceId",
                table: "Promotion",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ComerceId1",
                table: "Promotion",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Promotion_ComerceId1",
                table: "Promotion",
                column: "ComerceId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Promotion_Comerce_ComerceId1",
                table: "Promotion",
                column: "ComerceId1",
                principalTable: "Comerce",
                principalColumn: "ComerceId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
