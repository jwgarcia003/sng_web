﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace sng_web.Migrations
{
    public partial class user_comerce : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ComerceId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_ComerceId",
                table: "AspNetUsers",
                column: "ComerceId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Comerce_ComerceId",
                table: "AspNetUsers",
                column: "ComerceId",
                principalTable: "Comerce",
                principalColumn: "ComerceId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Comerce_ComerceId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_ComerceId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ComerceId",
                table: "AspNetUsers");
        }
    }
}
