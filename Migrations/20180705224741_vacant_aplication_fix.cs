﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace sng_web.Migrations
{
    public partial class vacant_aplication_fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Applicant",
                table: "VacantAplications",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ApplicantEmail",
                table: "VacantAplications",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ApplicantPhone",
                table: "VacantAplications",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Applicant",
                table: "VacantAplications");

            migrationBuilder.DropColumn(
                name: "ApplicantEmail",
                table: "VacantAplications");

            migrationBuilder.DropColumn(
                name: "ApplicantPhone",
                table: "VacantAplications");
        }
    }
}
