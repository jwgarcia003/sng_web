﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace sng_web.Migrations
{
    public partial class updatedatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cupon_Comerce_ComerceId1",
                table: "Cupon");

            migrationBuilder.DropForeignKey(
                name: "FK_Royalty_Comerce_ComerceId1",
                table: "Royalty");

            migrationBuilder.DropForeignKey(
                name: "FK_Vacant_Comerce_ComerceId1",
                table: "Vacant");

            migrationBuilder.DropIndex(
                name: "IX_Vacant_ComerceId1",
                table: "Vacant");

            migrationBuilder.DropIndex(
                name: "IX_Royalty_ComerceId1",
                table: "Royalty");

            migrationBuilder.DropIndex(
                name: "IX_Cupon_ComerceId1",
                table: "Cupon");

            migrationBuilder.DropColumn(
                name: "ComerceId1",
                table: "Vacant");

            migrationBuilder.DropColumn(
                name: "ComerceId1",
                table: "Royalty");

            migrationBuilder.DropColumn(
                name: "ComerceId1",
                table: "Cupon");

            migrationBuilder.AlterColumn<long>(
                name: "ComerceId",
                table: "Vacant",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<long>(
                name: "ComerceId",
                table: "Royalty",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<long>(
                name: "ComerceId",
                table: "Cupon",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_Vacant_ComerceId",
                table: "Vacant",
                column: "ComerceId");

            migrationBuilder.CreateIndex(
                name: "IX_Royalty_ComerceId",
                table: "Royalty",
                column: "ComerceId");

            migrationBuilder.CreateIndex(
                name: "IX_Cupon_ComerceId",
                table: "Cupon",
                column: "ComerceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cupon_Comerce_ComerceId",
                table: "Cupon",
                column: "ComerceId",
                principalTable: "Comerce",
                principalColumn: "ComerceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Royalty_Comerce_ComerceId",
                table: "Royalty",
                column: "ComerceId",
                principalTable: "Comerce",
                principalColumn: "ComerceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vacant_Comerce_ComerceId",
                table: "Vacant",
                column: "ComerceId",
                principalTable: "Comerce",
                principalColumn: "ComerceId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cupon_Comerce_ComerceId",
                table: "Cupon");

            migrationBuilder.DropForeignKey(
                name: "FK_Royalty_Comerce_ComerceId",
                table: "Royalty");

            migrationBuilder.DropForeignKey(
                name: "FK_Vacant_Comerce_ComerceId",
                table: "Vacant");

            migrationBuilder.DropIndex(
                name: "IX_Vacant_ComerceId",
                table: "Vacant");

            migrationBuilder.DropIndex(
                name: "IX_Royalty_ComerceId",
                table: "Royalty");

            migrationBuilder.DropIndex(
                name: "IX_Cupon_ComerceId",
                table: "Cupon");

            migrationBuilder.AlterColumn<int>(
                name: "ComerceId",
                table: "Vacant",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ComerceId1",
                table: "Vacant",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ComerceId",
                table: "Royalty",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ComerceId1",
                table: "Royalty",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ComerceId",
                table: "Cupon",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ComerceId1",
                table: "Cupon",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Vacant_ComerceId1",
                table: "Vacant",
                column: "ComerceId1");

            migrationBuilder.CreateIndex(
                name: "IX_Royalty_ComerceId1",
                table: "Royalty",
                column: "ComerceId1");

            migrationBuilder.CreateIndex(
                name: "IX_Cupon_ComerceId1",
                table: "Cupon",
                column: "ComerceId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Cupon_Comerce_ComerceId1",
                table: "Cupon",
                column: "ComerceId1",
                principalTable: "Comerce",
                principalColumn: "ComerceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Royalty_Comerce_ComerceId1",
                table: "Royalty",
                column: "ComerceId1",
                principalTable: "Comerce",
                principalColumn: "ComerceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vacant_Comerce_ComerceId1",
                table: "Vacant",
                column: "ComerceId1",
                principalTable: "Comerce",
                principalColumn: "ComerceId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
