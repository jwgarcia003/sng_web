﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace sng_web.Models
{
    public class VacantAplication
    {
        public int VacantAplicationId { get; set; }
        
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "¡Nombre del aspirante requerido!")]
        public String Applicant { get; set; }

        [Display(Name = "Telefono")]
        [DataType(DataType.PhoneNumber)]
        public String ApplicantPhone { get; set; }

        [Display(Name = "E-Mail")]
        [DataType(DataType.EmailAddress)]
        public String ApplicantEmail { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = false)]
        [Display(Name = "Fecha de Aplicación")]
        public DateTime AplicationDate { get; set; }

        [Required(ErrorMessage = "¡Se requiere que deje un comentario!")]
        [StringLength(200, MinimumLength = 20, ErrorMessage = "¡Comentario debe contener al menos 20 caracteres")]
        [Display(Name = "Breve comentario")]
        [DataType(DataType.MultilineText)]
        public String Comments { get; set; }

        [Display(Name = "Vacante")]
        public int? VacantId { get; set; }

        [ForeignKey("VacantId")]
        [Display(Name = "Vacante")]
        public Vacant Vacant { get; set; }

        [Display(Name = "Adjunto")]
        public string UrlCurriculum { get; set; }

        [Display(Name = "Revisada")]
        public Boolean Reviewed { get; set; }
        

        [StringLength(maximumLength: 200,  ErrorMessage = "¡Respuesta debe contener 200 caracteres máximo!")]
        [Display(Name = "Breve respuesta")]
        [DataType(DataType.MultilineText)]
        public String Response { get; set; }


        [Display(Name = "Usuario Aplicante")]
        public String UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

    }
}
