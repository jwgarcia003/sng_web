﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace sng_web.Models
{
    public class Image
    {
        public int ImageId { get; set; }
        public string UrlImage { get; set; }
        public ICollection<Proudct_Image> product_images { get; set; }
    }
    
    public class Proudct_Image
    {
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int ImageId { get; set; }
        public Image Image { get; set; }
        
    }

    public class Product
    {
        public int ProductId { get; set; }

        [Required(ErrorMessage = "¡Campo Nombre del producto requerido!")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "¡Nombre debe contener entre 5 y 50 caracteres!")]
        [Display(Name = "Producto")]
        public string Name { get; set; }

        [Required(ErrorMessage = "¡Campo Descripción del producto requerido!")]
        [StringLength(200, MinimumLength = 5, ErrorMessage = "¡Descripción debe contener entre 5 y 200 caracteres!")]
        [Display(Name = "Breve descripción")]
        public string Description { get; set; }

        [Required(ErrorMessage = "¡Campo Precio del producto requerido!")]
        [Display(Name = "Precio")]
        public Double Price { get; set; }

        [Display(Name = "Moneda")]
        public string Currency { get; set; }

        public string UrlImage { get; set; }

        public virtual ICollection<Proudct_Image> galeria { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = false)]
        [Display(Name = "Fecha de Creación")]
        public DateTime CreatedAt { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = false)]
        [Display(Name = "Última Actualización")]
        public DateTime UpdatedAt { get; set; }

        [Display(Name = "Habilitado")]
        public bool IsActived { get; set; }

        /*Relation ships*/
        //[Display(Name = "Comercio correspondiente")]
        [Display(Name = "Comercio")]
        public long? ComerceId { get; set; }

        [ForeignKey("ComerceId")]
        public virtual Comerce Comerce { get; set; }

        public virtual ICollection<User_Favorite_Products> user_favorites { get; set; }

    }
}
