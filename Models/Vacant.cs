﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace sng_web.Models
{
    public class Vacant
    {
        public int VacantId { get; set; }

        //public string UrlImage { get; set; }

        [Required(ErrorMessage = "¡Nombre de la vacante requerido!")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "¡Nombre de la vacante debe contener entre 5 y 50 caracteres!")]
        [Display(Name = "Vacante")]
        public string Name { get; set; }

        [Required(ErrorMessage = "¡Descripción de la vacante requerido!")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "¡Descripción de la vacante debe contener entre 5 y 50 caracteres!")]
        [Display(Name = "Descripción")]
        public string Description { get; set; }

        [StringLength(50, MinimumLength = 5, ErrorMessage = "¡Área de la vacante debe contener entre 5 y 50 caracteres!")]
        [Display(Name = "Área de la empresa")]
        public string Area { get; set; }

        [StringLength(50, MinimumLength = 5, ErrorMessage = "¡Cargo del ocupante debe contener entre 5 y 50 caracteres!")]
        [Display(Name = "Cargo del ocupante")]
        public string Cargo { get; set; }

        [Required(ErrorMessage = "¡Vacantes disponibles requeridas!")]
        [Display(Name = "Vacantes disponibles")]
        public int AvailableVacants { get; set; }

        [Required(ErrorMessage = "¡Horario de la vacante requerido!")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "¡Descripción de la vacante debe contener entre 5 y 50 caracteres!")]
        [Display(Name = "Horario")]
        public string schedule { get; set; }

        [Required(ErrorMessage = "¡Experiencia de la vacante requerido!")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "¡Campo Experiencia debe contener entre 2 y 100 caracteres!")]
        [Display(Name = "Experiencia necesaria")]
        public string Experience { get; set; }

        [Display(Name = "Genero")]
        public string Genre { get; set; }

        [Display(Name = "Edad minima necesaria")]
        public int Age { get; set; }

        [Display(Name = "Salario máximo")]
        public Double MaxSalary { get; set; }

        [Required(ErrorMessage = "¡Salario minimo requerido!")]
        [Display(Name = "Salario minimo")]
        public Double MinSalary { get; set; }

        [Required(ErrorMessage = "¡Seleccionar un departamento!")]
        [Display(Name = "Departamento del pais")]
        public string Department { get; set; }

        [Display(Name = "Pais")]
        public string Country { get; set; }

        [Display(Name = "Habilitado")]
        public Boolean IsActive { get; set; }

        /*Relation ships*/
        //[Display(Name = "Comercio correspondiente")]
        //[HiddenInput(DisplayValue = false)]
        [Display(Name = "Comercio")]
        public long? ComerceId { get; set; }

        [ForeignKey("ComerceId")]
        public virtual Comerce Comerce { get; set; }
    }
}
