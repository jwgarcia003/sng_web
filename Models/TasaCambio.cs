﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sng_web.Models
{
    public class TasaCambio
    {
        
        public int ID { get; set; }

        [Required(ErrorMessage = "¡Nombre del Banco requerido!")]
        [StringLength(200, MinimumLength = 2, ErrorMessage = "¡Nombre del Banco debe contener menos de 200 caracteres!")]
        [Display(Name = "Nombre del Banco")]
        public string Nombre_Banco { get; set; }

        [Required(ErrorMessage = "¡Precio de Compra requerido!")]
        [Display(Name = "Precio de Compra")]
        public Double Compra { get; set; }

        [Required(ErrorMessage = "¡Precio de venta requerido!")]
        [Display(Name = "Precio de venta")]
        public Double Venta { get; set; }


        [DataType(DataType.Date)]
        [Display(Name = "Fecha de Creación")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = false)]
        public DateTime Fecha { get; set; }
    }
}
