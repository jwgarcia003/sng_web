using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace sng_web.Models
{
    public class Notification
    {
        public int NotificationId { get; set; }

       
        [Required(ErrorMessage = "!Titulo de la notificación es requerido!")]
        [Display(Name = "Titulo de la Notificación")]
        public string Tittle { get; set; }

        [Required(ErrorMessage = "!Mensaje de la notificación es requerido!")]
        [Display(Name = "Mensaje")]
        public string Message { get; set; }



        [Display(Name = "Tipo de Modelo relacionado")]
        public string Model { get; set; }

        [Display(Name = "Modelo relacionado")]
        public string ModelReferenceId { get; set; }


        [DataType(DataType.Date)]
        [Display(Name = "Fecha de creación")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = false)]
        public DateTime CreatedAt { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha de envio")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = false)]
        public DateTime SentAt { get; set; }

    }
}
