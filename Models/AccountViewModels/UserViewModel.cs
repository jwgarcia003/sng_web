﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sng_web.Models.AccountViewModels
{
    public class RoleViewModel
    {
        public String Id { get; set; }
        public String Name { get; set; }
        public Boolean Selected { get; set; }
    }
    public class UserViewModel
    {
        public String Id { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String PhoneNumber { get; set; }
        public String AvatarURL { get; set; }
        public String Position { get; set; }
        public String NickName { get; set; }
        public String Email { get; set; }

        public ICollection<String> Roles { get; set; }

        public Comerce Comerce { get; set; }

        public long? ComerceId { get; set; }

        public UserViewModel()
        {
            Roles = new List<String>();
        }
    }
}
